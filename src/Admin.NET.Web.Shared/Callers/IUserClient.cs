﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IUserClient : IHttpApi
{
    /// <summary>
    /// 分页查询用户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysUser/page")]
    ITask<XnRestfulResult<PageResult<UserOutput>>> PageAsync(UserPageInput body, CancellationToken token = default);

    /// <summary>
    /// 增加用户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/add")]
    ITask<XnRestfulResult<string>> AddAsync([JsonContent] AddUserInput body, CancellationToken token = default);

    /// <summary>
    /// 删除用户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync([JsonContent] DeleteUserInput body, CancellationToken token = default);

    /// <summary>
    /// 更新用户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/edit")]
    ITask<XnRestfulResult<string>> EditAsync([JsonContent] UpdateUserInput body, CancellationToken token = default);

    /// <summary>
    /// 查看用户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysUser/detail")]
    ITask<XnRestfulResult<UserOutput>> DetailAsync(QueryUserInput body, CancellationToken token = default);

    /// <summary>
    /// 修改用户状态
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/changeStatus")]
    ITask<XnRestfulResult<string>> ChangeStatusAsync([JsonContent] UpdateUserStatusInput body, CancellationToken token = default);

    /// <summary>
    /// 授权用户角色
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/grantRole")]
    ITask<XnRestfulResult<string>> GrantRoleAsync([JsonContent] UpdateUserRoleDataInput body, CancellationToken token = default);

    /// <summary>
    /// 授权用户数据范围
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/grantData")]
    ITask<XnRestfulResult<string>> GrantDataAsync([JsonContent] UpdateUserRoleDataInput body, CancellationToken token = default);

    /// <summary>
    /// 更新用户信息
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/updateInfo")]
    ITask<XnRestfulResult<string>> UpdateInfoAsync([JsonContent] UpdateUserBaseInfoInput body, CancellationToken token = default);

    /// <summary>
    /// 修改用户密码
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/updatePwd")]
    ITask<XnRestfulResult<string>> UpdatePwdAsync([JsonContent] ChangePasswordUserInput body, CancellationToken token = default);

    /// <summary>
    /// 获取用户拥有角色
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysUser/ownRole")]
    ITask<XnRestfulResult<List<long>>> OwnRoleAsync(QueryUserInput body, CancellationToken token = default);

    /// <summary>
    /// 获取用户拥有数据
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysUser/ownData")]
    ITask<XnRestfulResult<List<long>>> OwnDataAsync(QueryUserInput body, CancellationToken token = default);

    /// <summary>
    /// 重置用户密码
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/resetPwd")]
    ITask<XnRestfulResult<string>> ResetPwdAsync([JsonContent] QueryUserInput body, CancellationToken token = default);

    /// <summary>
    /// 修改用户头像
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/updateAvatar")]
    ITask<XnRestfulResult<string>> UpdateAvatarAsync([JsonContent] UploadAvatarInput body, CancellationToken token = default);

    /// <summary>
    /// 获取用户选择器
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysUser/selector")]
    ITask<XnRestfulResult<List<UserOutput>>> SelectorAsync(UserSelectorInput body, CancellationToken token = default);

    /// <summary>
    /// 用户导出
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysUser/export")]
    [RawReturn]
    ITask<HttpResponseMessage> ExportAsync(CancellationToken token = default);

    /// <summary>
    /// 用户导入
    /// </summary>
    /// <param name="file"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysUser/import")]
    ITask<XnRestfulResult<string>> ImportAsync(FormDataFile file, CancellationToken token = default);
}
