﻿using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

public interface IOAuthClient : IHttpApi
{
    /// <summary>
    /// 微信登录授权
    /// </summary>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/oauth/wechat")]
    Task<XnRestfulResult<string>> WechatAsync(CancellationToken token = default);

    /// <summary>
    /// 微信登录授权回调
    /// </summary>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/oauth/wechatcallback")]
    Task<XnRestfulResult<string>> WechatcallbackAsync(string code, string state, string error_description, CancellationToken token = default);

    /// <summary>
    /// 获取微信用户基本信息
    /// </summary>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/oauth/wechat/user")]
    ITask<XnRestfulResult<object>> UserAsync(string token, string openId, CancellationToken cancellationToken = default);
}
