﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IMessageClient : IHttpApi
{
    /// <summary>
    /// 发送消息给所有人
    /// </summary>
    /// <param name="title">发送标题</param>
    /// <param name="message">发送内容</param>
    /// <param name="type">消息类型</param>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/sysMessage/allUser")]
    Task<XnRestfulResult<string>> AllUserAsync(string title, string message, MessageType? type, CancellationToken token = default);

    /// <summary>
    /// 发送消息给除了发送人的其他人
    /// </summary>
    /// <param name="title">发送标题</param>
    /// <param name="message">发送内容</param>
    /// <param name="type">消息类型</param>
    /// <param name="userId">发送人</param>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/sysMessage/otherUser")]
    Task<XnRestfulResult<string>> OtherUserAsync(string title, string message, MessageType? type, long? userId, CancellationToken token = default);

    /// <summary>
    /// 发送消息给某个人
    /// </summary>
    /// <param name="title">发送标题</param>
    /// <param name="message">发送内容</param>
    /// <param name="type">消息类型</param>
    /// <param name="userId">接收人</param>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/sysMessage/user")]
    Task<XnRestfulResult<string>> User2Async(string title, string message, MessageType? type, long? userId, CancellationToken token = default);

    /// <summary>
    /// 发送消息给某些人
    /// </summary>
    /// <param name="title">发送标题</param>
    /// <param name="message">发送内容</param>
    /// <param name="type">消息类型</param>
    /// <param name="body">接收人列表</param>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/sysMessage/users")]
    Task<XnRestfulResult<string>> UsersAsync(string title, string message, MessageType? type, IEnumerable<long> body, CancellationToken token = default);
}
