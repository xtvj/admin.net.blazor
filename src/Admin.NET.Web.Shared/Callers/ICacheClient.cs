﻿using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

public interface ICacheClient : IHttpApi
{
    /// <summary>
    /// 获取所有缓存关键字
    /// </summary>
    /// <returns>Success</returns>
    [HttpGet("api/sysCache/keyList")]
    ITask<XnRestfulResult<List<string>>> KeyListAsync(CancellationToken token = default);

    /// <summary>
    /// 删除指定关键字缓存
    /// </summary>
    /// <returns>Success</returns>
    [HttpGet("api/sysCache/remove")]
    Task<XnRestfulResult<string>> RemoveAsync(string key, CancellationToken token = default);

    /// <summary>
    /// 获取缓存
    /// </summary>
    /// <returns>Success</returns>
    [HttpGet("api/sysCache/detail")]
    ITask<XnRestfulResult<string>> DetailAsync(string cacheKey, CancellationToken token = default);
}
