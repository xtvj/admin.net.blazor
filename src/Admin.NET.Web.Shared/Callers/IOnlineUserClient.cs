﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IOnlineUserClient : IHttpApi
{
    /// <summary>
    /// 分页获取在线用户信息
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysOnlineUser/page")]
    ITask<XnRestfulResult<PageResult<OnlineUserOutput>>> PageAsync(OnlineUserPageInput body, CancellationToken token = default);

    /// <summary>
    /// 获取在线用户信息
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysOnlineUser/list")]
    ITask<XnRestfulResult<List<OnlineUserOutput>>> ListAsync(CancellationToken token = default);

    /// <summary>
    /// 强制下线
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysOnlineUser/forceExist")]
    ITask<XnRestfulResult<string>> ForceExistAsync([JsonContent] OnlineUserOutput body, CancellationToken token = default);
}
