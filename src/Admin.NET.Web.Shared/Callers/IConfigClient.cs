﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IConfigClient : IHttpApi
{
    /// <summary>
    /// 分页获取系统参数配置
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysConfig/page")]
    ITask<XnRestfulResult<PageResult<ConfigOutput>>> PageAsync(ConfigPageInput body, CancellationToken token = default);

    /// <summary>
    /// 获取系统参数配置列表
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysConfig/list")]
    ITask<XnRestfulResult<List<ConfigOutput>>> ListAsync(CancellationToken token = default);

    /// <summary>
    /// 增加系统参数配置
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysConfig/add")]
    ITask<XnRestfulResult<string>> AddAsync(AddConfigInput body, CancellationToken token = default);

    /// <summary>
    /// 删除系统参数配置
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysConfig/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(DeleteConfigInput body, CancellationToken token = default);

    /// <summary>
    /// 更新系统参数配置
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysConfig/edit")]
    ITask<XnRestfulResult<string>> EditAsync(UpdateConfigInput body, CancellationToken token = default);

    /// <summary>
    /// 获取系统参数配置
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysConfig/detail")]
    ITask<XnRestfulResult<ConfigOutput>> DetailAsync(QueryConfigInput body, CancellationToken token = default);

    /// <summary>
    /// 更新配置缓存
    /// </summary>
    /// <param name="code"></param>
    /// <param name="value"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPut("api/config/config-cache/{code}/{value}")]
    ITask<XnRestfulResult<string>> ConfigCacheAsync(string code, string value, CancellationToken token = default);
}
