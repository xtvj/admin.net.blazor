﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IDictDataClient : IHttpApi
{
    /// <summary>
    /// 分页查询字典值
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysDictData/page")]
    ITask<XnRestfulResult<PageResult<DictDataOutput>>> PageAsync(DictDataPageInput body, CancellationToken token = default);

    /// <summary>
    /// 获取某个字典类型下字典值列表
    /// </summary>
    /// <param name="typeId"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysDictData/list")]
    ITask<XnRestfulResult<List<DictDataOutput>>> ListAsync(long typeId, CancellationToken token = default);

    /// <summary>
    /// 增加字典值
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysDictData/add")]
    ITask<XnRestfulResult<string>> AddAsync(AddDictDataInput body, CancellationToken token = default);

    /// <summary>
    /// 删除字典值
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysDictData/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(DeleteDictDataInput body, CancellationToken token = default);

    /// <summary>
    /// 更新字典值
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysDictData/edit")]
    ITask<XnRestfulResult<string>> EditAsync(UpdateDictDataInput body, CancellationToken token = default);

    /// <summary>
    /// 字典值详情
    /// </summary>
    /// <param name="body"></param>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysDictData/detail")]
    ITask<XnRestfulResult<DictDataOutput>> DetailAsync(QueryDictDataInput body, CancellationToken token = default);

    /// <summary>
    /// 修改字典值状态
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysDictData/changeStatus")]
    ITask<XnRestfulResult<string>> ChangeStatusAsync(ChageStateDictDataInput body, CancellationToken token = default);
}
