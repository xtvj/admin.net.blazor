﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IDictTypeClient : IHttpApi
{
    /// <summary>
    /// 分页查询字典类型
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysDictType/page")]
    ITask<XnRestfulResult<PageResult<DictTypeOutput>>> PageAsync(DictTypePageInput body, CancellationToken token = default);

    /// <summary>
    /// 获取字典类型列表
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysDictType/list")]
    ITask<XnRestfulResult<List<DictTypeOutput>>> ListAsync(CancellationToken token = default);

    /// <summary>
    /// 获取字典类型下所有字典值
    /// </summary>
    /// <param name="code"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysDictType/dropDown")]
    ITask<XnRestfulResult<List<DictTypeOutput>>> DropDownAsync(string code, CancellationToken token = default);

    /// <summary>
    /// 添加字典类型
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysDictType/add")]
    ITask<XnRestfulResult<string>> AddAsync(AddDictTypeInput body, CancellationToken token = default);

    /// <summary>
    /// 删除字典类型
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysDictType/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(DeleteDictTypeInput body, CancellationToken token = default);

    /// <summary>
    /// 更新字典类型
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysDictType/edit")]
    ITask<XnRestfulResult<string>> EditAsync(UpdateDictTypeInput body, CancellationToken token = default);

    /// <summary>
    /// 字典类型详情
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysDictType/detail")]
    ITask<XnRestfulResult<DictTypeOutput>> DetailAsync(QueryDictTypeInput body, CancellationToken token = default);

    /// <summary>
    /// 更新字典类型状态
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysDictType/changeStatus")]
    ITask<XnRestfulResult<string>> ChangeStatusAsync(ChangeStateDictTypeInput body, CancellationToken token = default);

    /// <summary>
    /// 字典类型与字典值构造的字典树
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysDictType/tree")]
    ITask<XnRestfulResult<List<DictTreeOutput>>> TreeAsync(CancellationToken token = default);
}
