﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IFileClient : IHttpApi
{
    /// <summary>
    /// 分页获取文件列表
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysFileInfo/page")]
    ITask<XnRestfulResult<PageResult<FileOutput>>> PageAsync(FilePageInput body, CancellationToken token = default);

    /// <summary>
    /// 获取文件列表
    /// </summary>
    /// <param name="input"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysFileInfo/list")]
    ITask<XnRestfulResult<List<FileOutput>>> ListAsync(FileOutput input, CancellationToken token = default);

    /// <summary>
    /// 删除文件
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysFileInfo/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(DeleteFileInfoInput body, CancellationToken token = default);

    /// <summary>
    /// 获取文件详情
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysFileInfo/detail")]
    ITask<XnRestfulResult<FileOutput>> DetailAsync(QueryFileInfoInput body, CancellationToken token = default);

    /// <summary>
    /// 预览文件
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysFileInfo/preview")]
    ITask<HttpResponseMessage> PreviewAsync(QueryFileInfoInput body, CancellationToken token = default);

    /// <summary>
    /// 上传文件
    /// </summary>
    /// <param name="file"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysFileInfo/upload")]
    ITask<XnRestfulResult<long>> UploadAsync(FormDataFile file, CancellationToken token = default);

    /// <summary>
    /// 下载文件
    /// </summary>
    /// <param name="id">主键Id</param>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/sysFileInfo/download")]
    ITask<HttpResponseMessage> DownloadAsync(QueryFileInfoInput body, CancellationToken token = default);

    /// <summary>
    /// 上传头像
    /// </summary>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpPost("api/sysFileInfo/uploadAvatar")]
    ITask<XnRestfulResult<long>> UploadAvatarAsync(FormDataFile file, CancellationToken token = default);

    /// <summary>
    /// 上传文档
    /// </summary>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpPost("api/sysFileInfo/uploadDocument")]
    ITask<XnRestfulResult<string>> UploadDocumentAsync(FormDataFile file, CancellationToken token = default);

    /// <summary>
    /// 上传商店图片
    /// </summary>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpPost("api/sysFileInfo/uploadShop")]
    ITask<XnRestfulResult<string>> UploadShopAsync(FormDataFile file, CancellationToken token = default);
}
