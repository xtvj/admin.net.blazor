﻿using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IAuthClient : IHttpApi
{
    /// <summary>
    /// 用户登录
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/login")]
    [AllowAnonymous]
    ITask<XnRestfulResult<string>> LoginAsync([JsonContent] LoginInput body, CancellationToken token = default);

    /// <summary>
    /// 获取当前登录用户信息
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/getLoginUser")]
    ITask<XnRestfulResult<LoginOutput>> GetLoginUserAsync(CancellationToken token = default);

    /// <summary>
    /// 退出
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/logout")]
    ITask<XnRestfulResult<string>> LogoutAsync(CancellationToken token = default);

    /// <summary>
    /// 获取验证码开关
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/getCaptchaOpen")]
    ITask<XnRestfulResult<bool>> GetCaptchaOpenAsync(CancellationToken token = default);

    /// <summary>
    /// 获取验证码（默认点选模式）
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/captcha/get")]
    ITask<XnRestfulResult<string>> GetAsync(CancellationToken token = default);

    /// <summary>
    /// 校验验证码
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/captcha/check")]
    ITask<XnRestfulResult<string>> CheckAsync([JsonContent] ClickWordCaptchaInput body, CancellationToken token = default);
}
