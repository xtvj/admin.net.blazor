﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IRoleClient : IHttpApi
{
    /// <summary>
    /// 分页获取角色列表
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysRole/page")]
    ITask<XnRestfulResult<PageResult<RoleOutput>>> PageAsync(RolePageInput body, CancellationToken token = default);

    /// <summary>
    /// 角色下拉（用于授权角色时选择）
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysRole/dropDown")]
    ITask<XnRestfulResult<List<RoleOutput>>> DropDownAsync(CancellationToken token = default);

    /// <summary>
    /// 增加角色
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysRole/add")]
    ITask<XnRestfulResult<string>> AddAsync([JsonContent] AddRoleInput body, CancellationToken token = default);

    /// <summary>
    /// 删除角色
    /// </summary>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpPost("api/sysRole/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync([JsonContent] DeleteRoleInput body, CancellationToken token = default);

    /// <summary>
    /// 更新角色
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysRole/edit")]
    ITask<XnRestfulResult<string>> EditAsync([JsonContent] UpdateRoleInput body, CancellationToken token = default);

    /// <summary>
    /// 获取角色
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysRole/detail")]
    ITask<XnRestfulResult<RoleOutput>> DetailAsync(QueryRoleInput body, CancellationToken token = default);

    /// <summary>
    /// 授权角色菜单
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysRole/grantMenu")]
    ITask<XnRestfulResult<string>> GrantMenuAsync([JsonContent] GrantRoleMenuInput body, CancellationToken token = default);

    /// <summary>
    /// 授权角色数据范围
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysRole/grantData")]
    ITask<XnRestfulResult<string>> GrantData2Async([JsonContent] GrantRoleDataInput body, CancellationToken token = default);

    /// <summary>
    /// 获取角色拥有菜单Id集合
    /// </summary>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysRole/ownMenu")]
    ITask<XnRestfulResult<List<long>>> OwnMenuAsync(long id, CancellationToken token = default);

    /// <summary>
    /// 获取角色拥有数据Id集合
    /// </summary>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysRole/ownData")]
    ITask<XnRestfulResult<List<long>>> OwnDataAsync(long id, CancellationToken token = default);
}
