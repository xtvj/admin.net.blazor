﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IExLogClient : IHttpApi
{
    /// <summary>
    /// 分页查询异常日志
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysExLog/page")]
    ITask<XnRestfulResult<PageResult<ExLogOutput>>> PageAsync(ExLogPageInput body, CancellationToken token = default);

    /// <summary>
    /// 清空异常日志
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysExLog/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(CancellationToken token = default);
}
