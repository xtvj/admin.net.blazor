﻿using WebApiClientCore;

namespace Admin.NET.Web.Shared.Callers;

public class LogFilter : IApiFilter
{
    public Task OnRequestAsync(ApiRequestContext context)
    {
        return Task.CompletedTask;
    }

    public async Task OnResponseAsync(ApiResponseContext context)
    {
        Console.WriteLine($"----------------------------------------------------------------------");
        Console.WriteLine($"{context.ActionDescriptor.Name}：");
        Console.WriteLine($"{context.HttpContext.RequestMessage.Headers}");

        var result = "none";
        if (context.HttpContext.ResponseMessage != null)
        {
            result = await context.HttpContext.ResponseMessage.Content.ReadAsStringAsync();
        }
        Console.WriteLine($"{result}");
        Console.WriteLine($"----------------------------------------------------------------------");
    }
}
