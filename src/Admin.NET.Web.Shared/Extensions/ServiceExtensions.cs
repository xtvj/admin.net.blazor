﻿using Admin.NET.Web.Shared.Callers;

namespace Admin.NET.Web.Shared.Extensions;

public static class ServiceExtensions
{
    public static void RegisterAppServices(this IServiceCollection services,string baseAddress)
    {
        services.AddMasaBlazor(builder =>
        {
            builder.ConfigureTheme(theme =>
            {
                theme.Themes.Light.Primary = "#4318FF";
                theme.Themes.Light.Accent = "#4318FF";
                theme.Themes.Light.Error = "#FF5252";
                theme.Themes.Light.Success = "#00B42A";
                theme.Themes.Light.Warning = "#FF7D00";
                theme.Themes.Light.Info = "#37A7FF";
            });
        }).AddEnjoyBlazor();

        services.AddSingleton<WeatherForecastService>();
        services.AddSingleton<RouterPagesProvider>();
        services.AddBlazoredLocalStorage();
        services.AddAuthorizationCore();
        services
            .AddScoped<JwtAuthenticationStateProvider>()
            .AddScoped<IJwtStorageService>(x => x.GetRequiredService<JwtAuthenticationStateProvider>())
            .AddScoped<IJSInvokerService>(x => x.GetRequiredService<JwtAuthenticationStateProvider>())
            .AddScoped<AuthenticationStateProvider>(x => x.GetRequiredService<JwtAuthenticationStateProvider>())
            .AddScoped<GlobalConfig>();

        #region 注册webapi请求代理
        services.AddHttpApi<IAppClient>();
        services.AddHttpApi<IAuthClient>();
        services.AddHttpApi<ICacheClient>();
        services.AddHttpApi<IConfigClient>();
        services.AddHttpApi<IDictDataClient>();
        services.AddHttpApi<IDictTypeClient>();
        services.AddHttpApi<IEnumDataClient>();
        services.AddHttpApi<IExLogClient>();
        services.AddHttpApi<IFileClient>();
        services.AddHttpApi<IMachineClient>();
        services.AddHttpApi<IMenuClient>();
        services.AddHttpApi<IMessageClient>();
        services.AddHttpApi<INoticeClient>();
        services.AddHttpApi<IOAuthClient>();
        services.AddHttpApi<IOnlineUserClient>();
        services.AddHttpApi<IOpLogClient>();
        services.AddHttpApi<IOrgClient>();
        services.AddHttpApi<IPosClient>();
        services.AddHttpApi<IRoleClient>();
        services.AddHttpApi<ITenantClient>();
        services.AddHttpApi<ITimerClient>();
        services.AddHttpApi<IUserClient>();
        services.AddHttpApi<IVisLogClient>();
        services.AddWebApiClient().ConfigureHttpApi(x =>
        {
            //x.GlobalFilters.Add(new LogFilter());
            x.HttpHost = new Uri(baseAddress);
            x.Properties.Add("Cache-Control", "no-cache");
            x.Properties.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
        });
        #endregion

        #region 注册业务服务
        services.AddScoped<ISysAppService, SysAppService>();
        services.AddScoped<ISysAuthService, SysAuthService>();
        services.AddScoped<ISysMenuService, SysMenuService>();
        services.AddScoped<ISysExLogService, SysExLogService>();
        services.AddScoped<ISysVisLogService, SysVisLogService>();
        services.AddScoped<ISysOpLogService, SysOpLogService>();
        services.AddScoped<ISysConfigService, SysConfigService>();

        //services.AddScoped<ISysDictDataService, SysDictDataService>();
        //services.AddScoped<ISysDictTypeService, SysDictTypeService>();
        //services.AddScoped<ISysFileService, SysFileService>();
        //services.AddScoped<ISysMachineService, SysMachineService>();
        //services.AddScoped<ISysNoticeService, SysNoticeService>();
        //services.AddScoped<ISysOnlineUserService, SysOnlineUserService>();
        //services.AddScoped<ISysOrgService, SysOrgService>();
        //services.AddScoped<ISysPosService, SysPosService>();
        //services.AddScoped<ISysRoleService, SysRoleService>();
        //services.AddScoped<ISysTenantService, SysTenantService>();
        //services.AddScoped<ISysTimerService, SysTimerService>();
        //services.AddScoped<ISysUserService, SysUserService>();
        #endregion
    }
}

