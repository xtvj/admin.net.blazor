﻿namespace Admin.NET.Web.Shared.Extensions;

public static class EnumerableExtensions
{
    public static void AddRange<T>(this ICollection<T> list, IEnumerable<T>? collection)
    {
        if (collection is not null)
        {
            foreach (var item in collection)
            {
                list.Add(item);
            }
        }
    }
}
