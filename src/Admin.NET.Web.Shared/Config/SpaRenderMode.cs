﻿namespace Admin.NET.Web.Shared.Misc;

/// <summary>
/// 单页应用渲染模式
/// </summary>
public enum SpaRenderMode
{
    /// <summary>
    /// 服务端渲染
    /// </summary>
    Server,

    /// <summary>
    /// 客户端模式
    /// </summary>
    WebAssembly,

    /// <summary>
    /// 服务端预加载
    /// </summary>
    Automatic
}
