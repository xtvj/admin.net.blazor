﻿namespace Admin.NET.Web.Shared.Models;

public static class PageModes
{
    public const string PageTab = "PageTab";
    public const string Breadcrumb = "Breadcrumb";
}

