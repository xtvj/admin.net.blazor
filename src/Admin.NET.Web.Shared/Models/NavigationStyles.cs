﻿namespace Admin.NET.Web.Shared.Models;

public static class NavigationStyles
{
    public const string Flat = "Flat";
    public const string Rounded = "Rounded";
}
