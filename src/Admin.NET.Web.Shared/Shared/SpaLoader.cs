﻿using Microsoft.AspNetCore.Components;

namespace Admin.NET.Web.Shared.Shared;

/// <summary>
/// SPA数据加载组件
/// </summary>
public sealed class SpaLoader : IComponent, IHandleAfterRender, IDisposable
{
    private readonly RenderFragment _fragment;

    private bool _initialized;
    private bool _hasCalledOnAfterRender;
    private RenderHandle _renderHandle;
    private Task<AuthenticationState>? _currentStateTask;

    [Parameter]
    public RenderFragment? ChildContent { get; set; }

    [Inject]
    [NotNull]
    IJSRuntime? JS { get; set; }

    [Inject]
    [NotNull]
    JwtAuthenticationStateProvider? StateProvider { get; set; }

    [Inject]
    [NotNull]
    ISysAuthService? SysAuthService { get; set; }

    [Inject]
    [NotNull]
    WeatherForecastService? WeatherForecastService { get; set; }

    public SpaLoader()
    {
        _fragment = new RenderFragment(__builder =>
        {
            if (_hasCalledOnAfterRender)
            {
                __builder.OpenComponent<CascadingValue<Task<AuthenticationState>>>(0);
                __builder.AddAttribute(1, "Value", _currentStateTask);
                __builder.AddAttribute(2, "ChildContent", ChildContent);
                __builder.CloseComponent();
            }
        });
    }

    void IComponent.Attach(RenderHandle renderHandle)
    {
        if (_renderHandle.IsInitialized)
        {
            throw new InvalidOperationException("The render handle is already set. Cannot initialize a ComponentBase more than once.");
        }

        StateProvider.AuthenticationStateChanged += new(OnAuthenticationStateChanged);
        _renderHandle = renderHandle;
        _currentStateTask = StateProvider.GetAuthenticationStateAsync();
    }

    Task IComponent.SetParametersAsync(ParameterView parameters)
    {
        parameters.SetParameterProperties(this);

        if (!_initialized)
        {
            _initialized = true;
            _renderHandle.Render(new RenderFragment(x => { }));
        }

        return Task.CompletedTask;
    }

    async Task IHandleAfterRender.OnAfterRenderAsync()
    {
        if (!_hasCalledOnAfterRender)
        {
            _hasCalledOnAfterRender = true;

            if (!await LoadDataAsync(_currentStateTask))
            {
                await StateProvider.ClearAsync(true);
            }

            _renderHandle.Render(_fragment);

            await JS.NotifyAppIsReady();
            await JS.HideGlobalOverlay();
        }
    }

    void IDisposable.Dispose()
    {
        StateProvider.AuthenticationStateChanged -= new(OnAuthenticationStateChanged);
    }

    private async void OnAuthenticationStateChanged(Task<AuthenticationState> newAuthStateTask)
    {
        await LoadDataAsync(newAuthStateTask);
        await _renderHandle.Dispatcher.InvokeAsync(() =>
        {
            _currentStateTask = newAuthStateTask;
            _renderHandle.Render(_fragment);
        });
    }

    private async Task<bool> LoadDataAsync(Task<AuthenticationState>? authStateTask)
    {
        var _ = await WeatherForecastService.GetForecastAsync();

        await Task.Delay(3000);

        if (authStateTask is null)
        {
            return false;
        }

        var authState = await authStateTask;
        if (authState.User.Identity?.IsAuthenticated != true)
        {
            return false;
        }

        var result = await SysAuthService.GetLoginUserAsync();
        if (result.Success)
        {
            StateProvider.AddOrUpdateStorage(StorageConst.Local.CurrentApp, result.Data?.Apps?.FirstOrDefault());
            StateProvider.AddOrUpdateStorage(StorageConst.Local.CurrentAppItems, result.Data?.Apps);
            StateProvider.AddOrUpdateStorage(StorageConst.Local.CurrentNavMenus, result.Data?.Navs);
        }

        return result.Success;
    }
}
