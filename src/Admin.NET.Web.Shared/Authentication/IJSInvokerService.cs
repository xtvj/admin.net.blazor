﻿namespace Admin.NET.Web.Shared.Authentication;

public interface IJSInvokerService
{
    Task ConnectAsync(IJSInvoker? invoker, Func<HubConnection, Task>? signalRInitializer = null);

    Task DisconnectAsync();
}