﻿using Admin.NET.Web.Shared.Callers;

namespace Admin.NET.Web.Shared.Services;

public class SysOpLogService : BaseService, ISysOpLogService
{
    private readonly IOpLogClient _opLogClient;
    public SysOpLogService(IOpLogClient opLogClient, IPopupService popup) : base(popup)
        => _opLogClient = opLogClient;
    public Task<(int, IEnumerable<OpLogOutput>)> PageAsync(GridViewLoadDataArgs options)
        => PageAsync(_opLogClient.PageAsync(QuickAssign<OpLogPageInput>(options.CustomFilters)));
    public Task<bool> DeleteAsync()
        => ExecuteAsync(_opLogClient.DeleteAsync());
}

