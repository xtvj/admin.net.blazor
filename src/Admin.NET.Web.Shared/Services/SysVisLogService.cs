﻿using Admin.NET.Web.Shared.Callers;

namespace Admin.NET.Web.Shared.Services;

public class SysVisLogService : BaseService, ISysVisLogService
{
    private readonly IVisLogClient _visLogClient;
    public SysVisLogService(IVisLogClient visLogClient, IPopupService popup) : base(popup)
        => _visLogClient = visLogClient;
    public Task<(int, IEnumerable<VisLogOutput>)> PageAsync(GridViewLoadDataArgs options)
        => PageAsync(_visLogClient.PageAsync(QuickAssign<VisLogPageInput>(options.CustomFilters)));

    public Task<bool> DeleteAsync()
        => ExecuteAsync(_visLogClient.DeleteAsync());
}

