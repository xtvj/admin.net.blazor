﻿namespace Admin.NET.Web.Shared.Services.Interface;

public interface ISysVisLogService
{
    Task<bool> DeleteAsync();
    Task<(int, IEnumerable<VisLogOutput>)> PageAsync(GridViewLoadDataArgs options);
}
