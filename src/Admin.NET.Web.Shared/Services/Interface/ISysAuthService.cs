﻿namespace Admin.NET.Web.Shared.Services.Interface;

public interface ISysAuthService
{
    Task<XnRestfulResult<string>> LoginAsync(LoginInput input);

    Task<XnRestfulResult<LoginOutput>> GetLoginUserAsync();

    Task LogoutAsync();
}
