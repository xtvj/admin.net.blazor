﻿namespace Admin.NET.Web.Shared.Services.Interface;

public interface ISysMenuService
{
    Task<List<AppNavNodeOutput>?> ChangeAsync(string application);

    Task<List<MenuOutput>?> ListAsync(GetMenuListInput input);

    Task<List<MenuTreeOutput>?> TreeListAsync(string? application);

    Task<bool> AddAsync(MenuOutput input);

    Task<bool> SaveAsync(IEnumerable<MenuOutput> input, bool isAdd);

    Task<bool> DeleteAsync(IEnumerable<MenuOutput> input);

    Task<MenuOutput?> DetailAsync(QueryMenuInput input);

    Task<bool> EditAsync(MenuOutput input);
}

