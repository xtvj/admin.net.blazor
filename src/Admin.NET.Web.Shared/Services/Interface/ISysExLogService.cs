﻿namespace Admin.NET.Web.Shared.Services.Interface;

public interface ISysExLogService
{
    Task<bool> DeleteAsync();
    Task<(int, IEnumerable<ExLogOutput>)> PageAsync(GridViewLoadDataArgs options);
}
