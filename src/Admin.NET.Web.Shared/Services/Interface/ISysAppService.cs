﻿namespace Admin.NET.Web.Shared.Services.Interface;

public interface ISysAppService
{
    Task<(int, IEnumerable<AppOutput>)> PageAsync(GridViewLoadDataArgs options);

    Task<bool> DeleteAsync(IEnumerable<AppOutput> input);

    Task<bool> SaveAsync(IEnumerable<AppOutput> input, bool isAdd);

    Task<bool> SetAsDefaultAsync(AppOutput input);
}
