﻿namespace Enjoy.Blazor.SourceGenerator
{
    internal static class Constants
    {
        internal const string AuthorizeSingle = "AuthorizeResource";

        internal const string AuthorizeMultiple = "AuthorizeResources";

        internal const string AuthorizeResourceAttribute = @"
namespace Enjoy.Blazor
{
    /// <summary>
    /// 定义单个授权资源
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class AuthorizeResourceAttribute : Attribute
    {
        /// <summary>
        /// 资源名称
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// 许可
        /// </summary>
        public string Permission { get; }

        /// <summary>
        /// 分组
        /// </summary>
        public string? Group { get; }

        /// <summary>
        /// 描述
        /// </summary>
        public string? Description { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name=""name""></param>
        /// <param name=""permission""></param>
        /// <param name=""group""></param>
        /// <param name=""description""></param>
        public AuthorizeResourceAttribute(string name, string permission, string? group = null, string? description = null)
        {
            Name = name;
            Permission = permission;
            Group = group;
            Description = description;
        }
    }
}";

        internal const string AuthorizeResourcesAttribute = @"
namespace Enjoy.Blazor
{
    /// <summary>
    /// 定义多个授权资源
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class AuthorizeResourcesAttribute : Attribute
    {        
        /// <summary>
        /// 资源，格式为：资源名称|许可|分组|描述
        /// </summary>
        public string[] Resources { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name=""resources"">资源拼接字符串</param>
        public AuthorizeResourcesAttribute(params string[] resources)
        {
            Resources = resources;
        }
    }
}";
    }
}
