﻿using Admin.NET.Monitor.Filters;

namespace Admin.NET.Monitor;

public class ProfileViewProcessorBuilder
{
    private readonly List<IProfileFilter> _profileFilters = new();

    internal IEnumerable<IProfileFilter> ProfileFilters => _profileFilters.ToArray();

    public ProfileViewProcessorBuilder AddFilter(IProfileFilter filter)
    {
        _profileFilters.Add(filter);

        return this;
    }
}
