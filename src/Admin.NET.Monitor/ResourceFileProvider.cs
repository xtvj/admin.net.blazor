﻿using System.Reflection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;

namespace Admin.NET.Monitor;

internal class ResourceFileProvider : IResourceFileProvider
{
    private readonly EmbeddedFileProvider _embeddedFileProvider;

    public ResourceFileProvider()
    {
        _embeddedFileProvider = new EmbeddedFileProvider(Assembly.GetExecutingAssembly(), "Admin.NET.Web.Monitor");
    }

    public IDirectoryContents GetDirectoryContents(string subpath)
        => _embeddedFileProvider.GetDirectoryContents(subpath);


    public IFileInfo GetFileInfo(string subpath)
        => _embeddedFileProvider.GetFileInfo(subpath);


    public IChangeToken Watch(string filter)
        => _embeddedFileProvider.Watch(filter);

}
