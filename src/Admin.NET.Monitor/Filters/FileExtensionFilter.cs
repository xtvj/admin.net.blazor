﻿using System.Collections.Immutable;
using System.Diagnostics;

namespace Admin.NET.Monitor.Filters;

public class FileExtensionFilter : IProfileFilter
{
    private ImmutableArray<string> _filterExtensions;

    public FileExtensionFilter(params string[] filterExtensions)
    {
        if (filterExtensions is null)
            throw new ArgumentNullException(nameof(filterExtensions));

        _filterExtensions = filterExtensions.ToImmutableArray();
    }

    public bool Filtering(Activity activity)
        => _filterExtensions.Any(
            extension => activity.DisplayName.EndsWith($".{extension.TrimStart('.')}"));
}
