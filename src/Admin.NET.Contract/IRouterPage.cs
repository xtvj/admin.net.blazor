﻿using Admin.NET.Contract.Common;

namespace Admin.NET.Contract;

public interface IRouterPage
{
    static abstract string? Description { get; }

    static abstract IEnumerable<Permission> Authorities { get; }
}