﻿namespace Admin.NET.Contract;

/// <summary>
/// 是否
/// </summary>
public enum Sex
{
    /// <summary>
    /// 未知
    /// </summary>
    [Description("未知")]
    Unknow = 0,

    /// <summary>
    /// 男
    /// </summary>
    [Description("男")]
    Man = 1,

    /// <summary>
    /// 女
    /// </summary>
    [Description("女")]
    Women = 2
}
