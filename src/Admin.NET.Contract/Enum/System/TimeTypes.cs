﻿namespace Admin.NET.Contract;

/// <summary>
/// 任务类型
/// </summary>
public enum TimeTypes
{
    /// <summary>
    /// 间隔方式
    /// </summary>
    [Description("间隔方式")]
    Interval,

    /// <summary>
    /// Cron 表达式
    /// </summary>
    [Description("Cron 表达式")]
    Cron
}
