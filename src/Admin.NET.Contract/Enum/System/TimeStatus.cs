﻿namespace Admin.NET.Contract;

/// <summary>
/// 任务状态
/// </summary>
public enum TimeStatus
{
    /// <summary>
    /// 运行中
    /// </summary>
    [Description("运行中")]
    Running,

    /// <summary>
    /// 已停止或未启动
    /// </summary>
    [Description("已停止或未启动")]
    Stopped,

    /// <summary>
    /// 单次执行失败
    /// </summary>
    [Description("任务停止并失败")]
    Failed,

    /// <summary>
    /// 任务已取消或没有该任务
    /// </summary>
    [Description("任务已取消或没有该任务")]
    CanceledOrNone
}
