﻿namespace Admin.NET.Contract;

/// <summary>
/// 可见性
/// </summary>
public enum ShowOrHide
{
    /// <summary>
    /// 可见
    /// </summary>
    [Description("可见")]
    Y = 1,

    /// <summary>
    /// 不可见
    /// </summary>
    [Description("不可见")]
    N = 0
}
