﻿namespace Admin.NET.Contract;

/// <summary>
/// 查询条件
/// </summary>
[Serializable]
public class Condition
{
    /// <summary>
    /// 字段名
    /// </summary>
    public string? Field { get; set; }

    /// <summary>
    /// 操作符
    /// </summary>
    public QueryTypeEnum Op { get; set; }

    /// <summary>
    /// 字段值
    /// </summary>
    public object? Value { get; set; }

    /// <summary>
    /// 分组名称
    /// </summary>
    public string? OrGroup { get; set; }
}
