﻿namespace Admin.NET.Contract.System;

public class UpdateDictDataInput : BaseId<long>
{
    /// <summary>
    /// 字典类型Id
    /// </summary>
    [Required(ErrorMessage = "字典类型Id不能为空")]
    public long TypeId { get; set; }

    /// <summary>
    /// 值
    /// </summary>
    [Required(ErrorMessage = "字典值不能为空")]
    public string? Value { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "字典值编码不能为空")]
    public string? Code { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string? Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    public CommonStatus Status { get; set; }
}
