﻿namespace Admin.NET.Contract.System;

public class ChageStateDictDataInput : BaseId<long>
{
    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    public CommonStatus Status { get; set; }
}
