﻿namespace Admin.NET.Contract.System;

public class QueryDictDataListByCodeInput : BaseId<long>
{
    /// <summary>
    /// 字典类型Code
    /// </summary>
    [Required(ErrorMessage = "字典类型Code不能为空")]
    public string? Code { get; set; }
}
