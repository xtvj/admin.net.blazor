﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 字典类型参数
/// </summary>
public class DictTypePageInput : PageInputBase, IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public string? Code { get; set; }
}
