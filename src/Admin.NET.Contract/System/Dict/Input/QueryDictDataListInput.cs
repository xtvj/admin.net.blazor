﻿namespace Admin.NET.Contract.System;

public class QueryDictDataListInput : BaseId<long>
{
    /// <summary>
    /// 字典类型Id
    /// </summary>
    [Required(ErrorMessage = "字典类型Id不能为空")]
    public long TypeId { get; set; }
}
