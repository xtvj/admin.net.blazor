﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 字典值参数
/// </summary>
public class DictDataPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 字典类型Id
    /// </summary>
    public long TypeId { get; set; }

    /// <summary>
    /// 值
    /// </summary>
    public string? Value { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public string? Code { get; set; }
}
