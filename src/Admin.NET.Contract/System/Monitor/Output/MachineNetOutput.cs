﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 服务器网络信息
/// </summary>
public class MachineNetOutput : IDto
{
    [DisplayName("上下行流量统计")]
    public string? SendAndReceived { get; set; }
    [DisplayName("网络速度")]
    public string? NetworkSpeed { get; set; }
}
