﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 组织机构参数
/// </summary>
public class OrgListInput : IDto
{
    /// <summary>
    /// 父Id
    /// </summary>
    public long? Pid { get; set; }
}
