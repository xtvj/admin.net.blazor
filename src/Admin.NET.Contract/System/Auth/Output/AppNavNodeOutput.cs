﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 登录应用菜单节点    
/// </summary>
public class AppNavNodeOutput : BaseId<long>
{
    /// <summary>
    /// 父Id
    /// </summary>
    [DisplayName("上级菜单")]
    public long Pid { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("菜单名称")]
    [Required]
    public string? Name { get; set; }

    /// <summary>
    /// 菜单编号
    /// </summary>
    [DisplayName("菜单编号")]
    [Required]
    public string? Code { get; set; }

    /// <summary>
    /// 应用分类（应用编码）
    /// </summary>
    [DisplayName("所属应用")]
    [Required]
    public string? Application { get; set; }

    /// <summary>
    /// 图标
    /// </summary>
    [DisplayName("图标")]
    public string? Icon { get; set; }

    /// <summary>
    /// 路由地址
    /// </summary>
    [DisplayName("路由地址")]
    public string? Router { get; set; }

    /// <summary>
    /// 重定向地址
    /// </summary>
    [DisplayName("定向地址")]
    public string? Redirect { get; set; }

    /// <summary>
    /// 权限标识
    /// </summary>
    [DisplayName("权限标识")]
    public string? Permission { get; set; }

    /// <summary>
    /// 菜单类型（字典 0目录 1菜单 2按钮）
    /// </summary>
    [DisplayName("菜单类型")]
    [Required]
    public MenuType Type { get; set; }

    /// <summary>
    /// 打开方式（字典 0无 2内链 3外链）
    /// </summary>
    [DisplayName("打开方式")]
    public MenuOpenType OpenType { get; set; }

    /// <summary>
    /// 是否可见（Y-是，N-否）
    /// </summary>
    [DisplayName("是否可见")]
    public string? Visible { get; set; }

    /// <summary>
    /// 是否默认
    /// </summary>
    [DisplayName("是否默认")]
    public string? Active { get; set; }

    /// <summary>
    /// 权重（字典 1系统权重 2业务权重）
    /// </summary>
    [DisplayName("权重")]
    public MenuWeight Weight { get; set; } = MenuWeight.DEFAULT_WEIGHT;

    /// <summary>
    /// 排序
    /// </summary>
    [DisplayName("排序")]
    public int Sort { get; set; }
}