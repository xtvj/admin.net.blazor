﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 通知公告参数
/// </summary>
public class NoticePageInput : PageInputBase, IDto
{
    /// <summary>
    /// 类型（字典 1通知 2公告）
    /// </summary>
    public virtual int Type { get; set; }
}
