﻿namespace Admin.NET.Contract.System;

public class NoticeUserRead
{
    /// <summary>
    /// 用户Id
    /// </summary>
    public long UserId { get; set; }

    /// <summary>
    /// 用户名称
    /// </summary>
    public string? UserName { get; set; }

    /// <summary>
    /// 状态（字典 0未读 1已读）
    /// </summary>
    public NoticeUserStatus ReadStatus { get; set; }

    /// <summary>
    /// 阅读时间
    /// </summary>
    public DateTimeOffset? ReadTime { get; set; }
}