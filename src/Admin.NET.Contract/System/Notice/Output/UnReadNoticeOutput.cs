﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 系统通知公告详情参数
/// </summary>
public class UnReadNoticeOutput
{
    public int Total { get; set; }

    /// <summary>
    /// 通知到的用户阅读信息集合
    /// </summary>
    public List<UnReadNoticeItem> UnReadGroups { get; set; } = new();
}

/// <summary>
/// 系统通知公告详情参数
/// </summary>
public class UnReadNoticeItem
{
    public int Index { get; set; }

    public string? Key { get; set; }

    public int Value { get; set; }

    /// <summary>
    /// 通知到的用户阅读信息集合
    /// </summary>
    public List<NoticeReceiveOutput> NoticeData { get; set; } = new();
}
