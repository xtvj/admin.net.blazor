﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 请求日志参数
/// </summary>
public class OpLogOutput : IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 是否执行成功（Y-是，N-否）
    /// </summary>
    [DisplayName("是否成功")]
    public YesOrNot Success { get; set; }

    /// <summary>
    /// 具体消息
    /// </summary>
    [DisplayName("具体消息")]
    public string? Message { get; set; }

    /// <summary>
    /// ip
    /// </summary>
    [DisplayName("IP地址")]
    public string? Ip { get; set; }

    /// <summary>
    /// 地址
    /// </summary>
    [DisplayName("地址")]
    public string? Location { get; set; }

    /// <summary>
    /// 浏览器
    /// </summary>
    [DisplayName("浏览器")]
    public string? Browser { get; set; }

    /// <summary>
    /// 操作系统
    /// </summary>
    [DisplayName("操作系统")]
    public string? Os { get; set; }

    /// <summary>
    /// 请求地址
    /// </summary>
    [DisplayName("请求地址")]
    public string? Url { get; set; }

    /// <summary>
    /// 类名称
    /// </summary>
    [DisplayName("类名称")]
    public string? ClassName { get; set; }

    /// <summary>
    /// 方法名称
    /// </summary>
    [DisplayName("方法名称")]
    public string? MethodName { get; set; }

    /// <summary>
    /// 请求方式（GET POST PUT DELETE)
    /// </summary>
    [DisplayName("请求方式")]
    public string? ReqMethod { get; set; }

    /// <summary>
    /// 请求参数
    /// </summary>
    [DisplayName("请求参数")]
    public string? Param { get; set; }

    /// <summary>
    /// 返回结果
    /// </summary>
    [DisplayName("返回结果")]
    public string? Result { get; set; }

    /// <summary>
    /// 耗时（毫秒）
    /// </summary>
    [DisplayName("耗时")]
    public long ElapsedTime { get; set; }

    /// <summary>
    /// 操作时间
    /// </summary>
    [DisplayName("操作时间")]
    public DateTimeOffset? OpTime { get; set; }

    /// <summary>
    /// 操作人
    /// </summary>
    [DisplayName("操作人")]
    public string? Account { get; set; }
}