﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 异常日志参数
/// </summary>
public class ExLogOutput : IDto
{
    /// <summary>
    /// 操作人
    /// </summary>
    [DisplayName("操作人")]
    public string? Account { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 类名
    /// </summary>
    [DisplayName("类名")]
    public string? ClassName { get; set; }

    /// <summary>
    /// 方法名
    /// </summary>
    [DisplayName("方法名")]
    public string? MethodName { get; set; }

    /// <summary>
    /// 异常名称
    /// </summary>
    [DisplayName("异常名称")]
    public string? ExceptionName { get; set; }

    /// <summary>
    /// 异常信息
    /// </summary>
    [DisplayName("异常信息")]
    public string? ExceptionMsg { get; set; }

    /// <summary>
    /// 异常时间
    /// </summary>
    [DisplayName("异常时间")]
    public DateTimeOffset? ExceptionTime { get; set; }

    /// <summary>
    /// 异常时间查询范围 yyyy-MM-dd HH:mm:ss~yyyy-MM-dd HH:mm:ss
    /// </summary>
    [DisplayName("异常时间")]
    public string? SearchTimeRange { get; set; }
}