namespace Admin.NET.Contract.System;

/// <summary>
/// 访问日志参数
/// </summary>
public class VisLogPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 是否执行成功（Y-是，N-否）
    /// </summary>
    [DisplayName("是否成功")]
    public YesOrNot? Success { get; set; }

    /// <summary>
    /// 访问类型
    /// </summary>
    [DisplayName("访问类型")]
    public LoginType? VisType { get; set; }
}