﻿namespace Admin.NET.Contract.System;

public class AddAppInput : IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    [Required(ErrorMessage = "名称不能为空")]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [DisplayName("编码")]
    [Required(ErrorMessage = "编码不能为空")]
    public string? Code { get; set; }

    /// <summary>
    /// 图标
    /// </summary>
    [DisplayName("图标")]
    [Required(ErrorMessage = "图标不能为空")]
    public string? Icon { get; set; }

    /// <summary>
    /// 是否默认激活（Y-是，N-否）,只能有一个系统默认激活
    /// <br/>用户登录后默认展示此系统菜单
    /// </summary>
    [DisplayName("是否默认激活")]
    public string Active { get; set; } = "N";

    [DisplayName("状态")]
    public CommonStatus Status { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    [DisplayName("排序")]
    public int Sort { get; set; }
}
