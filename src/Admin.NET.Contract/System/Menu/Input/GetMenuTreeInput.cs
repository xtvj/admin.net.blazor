﻿namespace Admin.NET.Contract.System;

public class GetMenuTreeInput : IDto
{
    /// <summary>
    /// 应用分类（应用编码）
    /// </summary>
    public string? Application { get; set; }
}
