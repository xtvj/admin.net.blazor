﻿using System.Text.Json.Serialization;

namespace Admin.NET.Contract.System;

/// <summary>
/// 任务信息---任务详情
/// </summary>
public class JobOutput : BaseId<long>,INotifyPropertyChanged
{
    private RequestTypeEnum _requestType;
    private TimeTypes _timerType;

    /// <summary>
    /// 已执行次数
    /// </summary>
    [DisplayName("执行次数")]
    public long RunNumber { get; set; }

    /// <summary>
    /// 定时器状态
    /// </summary>
    [DisplayName("运行状态")]
    public TimeStatus TimerStatus { get; set; } = TimeStatus.Stopped;

    /// <summary>
    /// 异常信息
    /// </summary>
    [DisplayName("异常信息")]
    public string? Exception { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    [DisplayName("创建时间")]
    public DateTimeOffset? CreatedTime { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    [DisplayName("更新时间")]
    public DateTimeOffset? UpdatedTime { get; set; }

    /// <summary>
    /// 创建者Id
    /// </summary>
    [DisplayName("创建者Id")]
    public long? CreatedUserId { get; set; }

    /// <summary>
    /// 创建者名称
    /// </summary>
    [DisplayName("创建者名称")]
    [StringLength(20)]
    public string? CreatedUserName { get; set; }

    /// <summary>
    /// 修改者Id
    /// </summary>
    [DisplayName("修改者Id")]
    public long? UpdatedUserId { get; set; }

    /// <summary>
    /// 修改者名称
    /// </summary>
    [DisplayName("修改者名称")]
    [StringLength(20)]
    public string? UpdatedUserName { get; set; }

    /// <summary>
    /// 任务名称
    /// </summary>
    [DisplayName("任务名称")]
    [Required(ErrorMessage = "任务名称不能为空")]
    [StringLength(20)]
    public string? JobName { get; set; }

    /// <summary>
    /// 只执行一次
    /// </summary>
    [DisplayName("只执行一次")]
    public bool DoOnce { get; set; }

    /// <summary>
    /// 立即执行（默认等待启动）
    /// </summary>
    [DisplayName("立即执行（默认等待启动）")]
    public bool StartNow { get; set; }

    /// <summary>
    /// 立即执行（默认等待启动）
    /// </summary>
    [DisplayName("执行类型")]
    [JsonPropertyName("executeType")]
    public TimeExecuteTypes ExecuteType { get; set; }

    /// <summary>
    /// 执行间隔时间（单位秒）
    /// </summary>
    [DisplayName("执行间隔时间（单位秒）")]
    public int? Interval { get; set; }

    /// <summary>
    /// Cron表达式
    /// </summary>
    [DisplayName("Cron表达式")]
    [StringLength(20)]
    public string? Cron { get; set; }

    [DisplayName("任务类型")]
    public TimeTypes TimerType
    {
        get => _timerType;
        set
        {
            _timerType = value;
            NotifyPropertyChanged(nameof(TimerType));
        }
    }

    /// <summary>
    /// 请求url
    /// </summary>
    [DisplayName("请求地址")]
    [StringLength(200)]
    public string? RequestUrl { get; set; }

    /// <summary>
    /// 请求参数（Post，Put请求用）
    /// </summary>
    [DisplayName("请求参数")]
    public string? RequestParameters { get; set; }

    /// <summary>
    /// Headers(可以包含如：Authorization授权认证)
    /// <br/>格式：{"Authorization":"userpassword.."}
    /// </summary>
    [DisplayName("请求头")]
    public string? Headers { get; set; }

    /// <summary>
    /// 请求类型
    /// </summary>
    [DisplayName("请求类型")]
    public RequestTypeEnum RequestType
    {
        get => _requestType;
        set
        {
            _requestType = value;
            NotifyPropertyChanged(nameof(RequestType));
        }
    }

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    [StringLength(100)]
    public string? Remark { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>
    /// 通知属性发生改变
    /// </summary>
    /// <param name="propertyName"></param>
    public virtual void NotifyPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
