﻿namespace Admin.NET.Contract.System;

public class QueryEnumDataInput : BaseId<long>
{
    /// <summary>
    /// 实体名称
    /// </summary>
    [Required(ErrorMessage = "实体名称不能为空")]
    public string? EntityName { get; set; }

    /// <summary>
    /// 字段名称
    /// </summary>
    [Required(ErrorMessage = "字段名称不能为空")]
    public string? FieldName { get; set; }
}