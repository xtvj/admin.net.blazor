﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 更新用户授权数据角色和数据范围
/// </summary>
public class UpdateUserRoleDataInput : BaseId<long>, IXnInputBase
{
    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput2 SysEmpParam { get; set; } = new EmpOutput2();

    public List<long> GrantMenuIdList { get; set; } = new();
    public List<long> GrantRoleIdList { get; set; } = new();
    public List<long> GrantOrgIdList { get; set; } = new();
}
