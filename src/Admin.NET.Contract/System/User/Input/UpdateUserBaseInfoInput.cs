﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 更新用户基本信息
/// </summary>
public class UpdateUserBaseInfoInput : BaseId<long>
{
    [Required(ErrorMessage = "昵称不能为空")]
    public string? NickName { get; set; }

    [Required(ErrorMessage = "手机号不能为空")]
    public string? Phone { get; set; }

    [Required(ErrorMessage = "电子邮箱不能为空")]
    public string? Email { get; set; }

    [Required(ErrorMessage = "性别不能为空")]
    public int Sex { get; set; }

    [Required(ErrorMessage = "生日不能为空")]
    public DateTime? Birthday { get; set; }

    public string? Tel { get; set; }
}
