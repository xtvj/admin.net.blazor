﻿namespace Admin.NET.Contract.System;

public class UploadAvatarInput : BaseId<long>
{
    /// <summary>
    /// 头像文件路径标识
    /// </summary>
    [Required(ErrorMessage = "头像文件路径标识不能为空")]
    public long Avatar { get; set; }
}