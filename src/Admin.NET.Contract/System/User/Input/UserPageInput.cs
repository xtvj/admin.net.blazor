namespace Admin.NET.Contract.System;

/// <summary>
/// 用户参数
/// </summary>
public class UserPageInput : PageInputBase, IXnInputBase, IDto
{
    /// <summary>
    /// 账号
    /// </summary>
    public string? Account { get; set; }

    /// <summary>
    /// 密码
    /// </summary>
    public string? Password { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    public string? NickName { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// 头像
    /// </summary>
    public string? Avatar { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    public DateTime? Birthday { get; set; }

    /// <summary>
    /// 性别-男_1、女_2
    /// </summary>
    public int Sex { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    public string? Email { get; set; }

    /// <summary>
    /// 手机
    /// </summary>
    public string? Phone { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public string? Tel { get; set; }

    /// <summary>
    /// 状态-正常_0、停用_1、删除_2
    /// </summary>
    public CommonStatus Status { get; set; }

    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput2 SysEmpParam { get; set; } = new EmpOutput2();

    /// <summary>
    /// 搜索状态（字典 0正常 1停用 2删除）
    /// </summary>
    public CommonStatus SearchStatus { get; set; } = CommonStatus.ENABLE;

    public List<long> GrantMenuIdList { get; set; } = new();
    public List<long> GrantRoleIdList { get; set; } = new();
    public List<long> GrantOrgIdList { get; set; } = new();
}
