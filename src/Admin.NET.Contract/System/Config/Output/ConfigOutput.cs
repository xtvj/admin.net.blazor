﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 参数配置表
/// </summary>
[DisplayName("参数配置表")]
public class ConfigOutput : BaseId<long>
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    [Required, MaxLength(50)]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [DisplayName("编码")]
    [Required, MaxLength(50)]
    public string? Code { get; set; }

    /// <summary>
    /// 属性值
    /// </summary>
    [DisplayName("属性值")]
    public string? Value { get; set; }

    /// <summary>
    /// 常量所属分类的编码，来自于“常量的分类”字典
    /// </summary>
    [DisplayName("所属分类")]
    [MaxLength(50)]
    public string? GroupCode { get; set; }

    /// <summary>
    /// 是否是系统参数（Y-是，N-否）
    /// </summary>
    [DisplayName("系统参数")]
    [MaxLength(5)]
    public string? SysFlag { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    [DisplayName("状态")]
    public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    [MaxLength(100)]
    public string? Remark { get; set; }
}