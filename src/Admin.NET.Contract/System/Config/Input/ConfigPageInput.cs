﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 参数配置
/// </summary>
public class ConfigPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    public virtual string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public virtual string? Code { get; set; }

    /// <summary>
    /// 常量所属分类的编码，来自于“常量的分类”字典
    /// </summary>
    public virtual string? GroupCode { get; set; }
}