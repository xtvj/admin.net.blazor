﻿namespace Admin.NET.Contract.System;

public class UpdateConfigInput : BaseId<long>
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "参数名称不能为空")]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "参数编码不能为空")]
    public string? Code { get; set; }

    /// <summary>
    /// 属性值
    /// </summary>
    public string? Value { get; set; }

    /// <summary>
    /// 是否是系统参数（Y-是，N-否）
    /// </summary>
    public string? SysFlag { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string? Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// 常量所属分类的编码，来自于“常量的分类”字典
    /// </summary>
    public string? GroupCode { get; set; }
}