﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 租户参数
/// </summary>
public class TenantPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 公司名称
    /// </summary>
    public virtual string? Name { get; set; }

    /// <summary>
    /// 主机
    /// </summary>
    public virtual string? Host { get; set; }
}