﻿namespace Admin.NET.Contract.System;

public class AddRoleInput : IDto
{
    /// <summary>
    /// 角色类型-集团角色_0、加盟商角色_1、门店角色_2
    /// </summary>
    [Required(ErrorMessage = "请选择角色类型")]
    public RoleTypeEnum RoleType { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    [Required(ErrorMessage = "角色名称不能为空")]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "角色编码不能为空")]
    public string? Code { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 数据范围类型（字典 1全部数据 2本部门及以下数据 3本部门数据 4仅本人数据 5自定义数据）
    /// </summary>
    public int DataScopeType { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string? Remark { get; set; }
}
