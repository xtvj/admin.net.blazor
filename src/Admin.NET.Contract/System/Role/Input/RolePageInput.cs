﻿namespace Admin.NET.Contract.System;

public class RolePageInput : PageInputBase, IDto
{
    /// <summary>
    /// 角色类型-集团角色_0、加盟商角色_1、门店角色_2
    /// </summary>
    [Required(ErrorMessage = "请选择角色类型")]
    public RoleTypeEnum RoleType { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public virtual string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public virtual string? Code { get; set; }
}
