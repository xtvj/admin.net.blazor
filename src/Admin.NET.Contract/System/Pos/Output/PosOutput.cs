﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 职位表
/// </summary>
[DisplayName("职位表")]
public class PosOutput : BaseId<long>
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    [Required, MaxLength(20)]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [DisplayName("编码")]
    [Required, MaxLength(50)]
    public string? Code { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    [DisplayName("排序")]
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    [MaxLength(100)]
    public string? Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    [DisplayName("状态")]
    public CommonStatus Status { get; set; } = CommonStatus.ENABLE;
}