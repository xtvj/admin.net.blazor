﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 职位参数
/// </summary>
public class PosPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    public virtual string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public virtual string? Code { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    public string? Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    public int Status { get; set; }
}
