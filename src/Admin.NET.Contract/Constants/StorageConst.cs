﻿namespace Admin.NET.Contract.Constants;

public static class StorageConst
{
    public static class Local
    {
        public const string AccessToken = "authToken";
        public const string RefreshToken = "refreshToken";
        public const string UserImageURL = "userImageURL";

        public const string CurrentApp = "CurrentApp";
        public const string CurrentAppItems = "CurrentAppItems";
        public const string CurrentNavMenus = "CurrentNavMenus";
    }
}