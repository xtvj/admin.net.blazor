﻿using Admin.NET.Contract.Common;

namespace Admin.NET.Contract;

/// <summary>
/// 权限点扩展
/// </summary>
public static class PermissionExtensions
{
    /// <summary>
    /// 定义权限点
    /// </summary>
    /// <param name="items"></param>
    /// <param name="code">静态编码，必须硬编码，不能动态生成</param>
    /// <param name="name">简称</param>
    /// <param name="description">描述</param>
    /// <returns>权限点</returns>
    public static Permission Define(this ICollection<Permission> items, string code, string name, string? description)
    {
        var permission = new Permission(code, name, description);
        items.Add(permission);
        return permission;
    }

    /// <summary>
    /// 定义权限点
    /// </summary>
    /// <param name="items"></param>
    /// <param name="code">静态编码，必须硬编码，不能动态生成</param>
    /// <param name="name">简称</param>
    /// <returns>权限点</returns>
    public static Permission Define(this ICollection<Permission> items, string code, string name)
    {
        return Define(items, code, name, null);
    }
}