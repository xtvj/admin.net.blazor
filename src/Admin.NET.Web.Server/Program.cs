using Admin.NET.Web.Shared;
using Admin.NET.Web.Shared.Extensions;
using Admin.NET.Web.Shared.Misc;
using Microsoft.AspNetCore.Components.Web;

var builder = WebApplication.CreateBuilder(args);
var apiBaseAddress = builder.Configuration.GetValue<string>(nameof(PublicOptions.ApiBaseAddress));
if (string.IsNullOrWhiteSpace(apiBaseAddress))
{
    apiBaseAddress = "http://localhost:9001";
}

builder.Services.AddOptions();
builder.Services.Configure<PublicOptions>(builder.Configuration);
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor(configure => configure.RootComponents.RegisterForJavaScript<App>("server"));
builder.Services.RegisterAppServices(apiBaseAddress);

var app = builder.Build();
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}
app.UseHttpsRedirection();
app.UseBlazorFrameworkFiles();
app.UseStaticFiles();
app.UseRouting();
app.MapBlazorHub();
app.MapGet("/ApiBaseAddress", () => apiBaseAddress);
app.MapFallbackToPage("/_host");
app.Run();