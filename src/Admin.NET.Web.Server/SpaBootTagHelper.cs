﻿using Admin.NET.Web.Shared.Misc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Options;
using System.Diagnostics.CodeAnalysis;

namespace Admin.NET.Web.Server;

/// <summary>
/// blazory应用注册点
/// </summary>
[HtmlTargetElement("boot",Attributes = "asp-append-version")]
public class SpaBootTagHelper : TagHelper
{
    [HtmlAttributeNotBound]
    [ViewContext]
    [NotNull]
    public ViewContext? ViewContext { get; set; }

    [HtmlAttributeName("asp-append-version")]
    public bool? Version { get; set; }

    public override void Process(TagHelperContext context, TagHelperOutput output)
    {
        SpaRenderMode mode;
        var path = ViewContext.HttpContext.Request.Path.Value?.ToLower();
        if (path == "/_host/webAssembly")
        {
            mode = SpaRenderMode.WebAssembly;
        }
        else if (path == "/_host/server")
        {
            mode = SpaRenderMode.Server;
        }
        else
        {
            var options = ViewContext.HttpContext.RequestServices.GetRequiredService<IOptions<PublicOptions>>();
            mode = options.Value.SpaRenderMode;
        }

        var provider = ViewContext.HttpContext.RequestServices.GetRequiredService<IFileVersionProvider>();
        var pathBase = ViewContext.HttpContext.Request.PathBase;

        var style = new TagBuilder("link");
        style.Attributes["rel"] = "stylesheet";
        style.Attributes["href"] = provider.AddFileVersionToPath(pathBase, "/blazor.web.css");
        style.RenderSelfClosingTag();
        output.PreElement.SetHtmlContent(style);

        output.TagName = "script";
        output.Attributes.SetAttribute("id", "blazor-spa-booter");
        output.Attributes.SetAttribute("src", provider.AddFileVersionToPath(pathBase, "/blazor.web.js"));
        output.Attributes.SetAttribute("data-mode", mode);
    }
}