﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysEmpExtOrgPosService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="empId"></param>
    /// <param name="extIdList"></param>
    /// <returns></returns>
    Task AddOrUpdate(long empId, List<EmpExtOrgPosOutput> extIdList);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="empId"></param>
    /// <returns></returns>
    Task DeleteEmpExtInfoByUserId(long empId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="empId"></param>
    /// <returns></returns>
    Task<List<EmpExtOrgPosOutput>> GetEmpExtOrgPosList(long empId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="orgId"></param>
    /// <returns></returns>
    Task<bool> HasExtOrgEmp(long orgId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="posId"></param>
    /// <returns></returns>
    Task<bool> HasExtPosEmp(long posId);
}