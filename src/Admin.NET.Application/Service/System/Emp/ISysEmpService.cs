﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysEmpService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sysEmpParam"></param>
    /// <returns></returns>
    Task AddOrUpdate(EmpOutput2 sysEmpParam);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="empId"></param>
    /// <returns></returns>
    Task DeleteEmpInfoByUserId(long empId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="empId"></param>
    /// <returns></returns>
    Task<EmpOutput> GetEmpInfo(long empId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="empId"></param>
    /// <returns></returns>
    Task<long> GetEmpOrgId(long empId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="orgId"></param>
    /// <returns></returns>
    Task<bool> HasOrgEmp(long orgId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="orgId"></param>
    /// <param name="orgName"></param>
    /// <returns></returns>
    Task UpdateEmpOrgInfo(long orgId, string orgName);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="orgIds"></param>
    /// <returns></returns>
    Task<List<SysEmp>> HasOrgEmp(List<long> orgIds);
}