﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface IGeneralCaptcha
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    ClickWordCaptchaResult CheckCode(GeneralCaptchaInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="length"></param>
    /// <returns></returns>
    ClickWordCaptchaResult CreateCaptchaImage(int length = 4);
}