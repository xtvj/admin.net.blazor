﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysDictDataService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task AddDictData(AddDictDataInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task ChangeDictDataStatus(ChageStateDictDataInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dictTypeId"></param>
    /// <returns></returns>
    Task DeleteByTypeId(long dictTypeId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task DeleteDictData(DeleteDictDataInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<DictDataOutput> GetDictData([FromQuery] QueryDictDataInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<DictDataOutput>> GetDictDataList([FromQuery] QueryDictDataListInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dictTypeId"></param>
    /// <returns></returns>
    Task<List<DictDataOutput>> GetDictDataListByDictTypeId(long dictTypeId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<DictDataOutput>> QueryDictDataPageList([FromQuery] DictDataPageInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateDictData(UpdateDictDataInput input);
}