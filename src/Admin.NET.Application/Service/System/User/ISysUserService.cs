﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysUserService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task AddUser(AddUserInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task ChangeUserStatus(UpdateUserStatusInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task DeleteUser(DeleteUserInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<IActionResult> ExportUser();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<UserOutput> GetUser(long id);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    Task<SysUser> GetUserById(long userId);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<long>> GetUserDataScopeIdList();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    Task<List<long>> GetUserDataScopeIdList(long userId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<long>> GetUserOwnData([FromQuery] QueryUserInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<long>> GetUserOwnRole([FromQuery] QueryUserInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<UserOutput>> GetUserSelector([FromQuery] UserSelectorInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task GrantUserData(UpdateUserRoleDataInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task GrantUserRole(UpdateUserRoleDataInput input);

    /// <summary>
    /// 获取用户列表
    /// </summary>
    /// <returns></returns>
    Task<List<UserOutput>> GetSysUserList();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    Task ImportUser(IFormFile file);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<UserOutput>> QueryUserPageList([FromQuery] UserPageInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task ResetUserPwd(QueryUserInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="authUser"></param>
    /// <param name="sysUser"></param>
    /// <returns></returns>
    Task SaveAuthUserToUser(AuthUserInput authUser, CreateUserInput sysUser);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateAvatar(UploadAvatarInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateUser(UpdateUserInput input);
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateUserInfo(UpdateUserBaseInfoInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateUserPwd(ChangePasswordUserInput input);
}