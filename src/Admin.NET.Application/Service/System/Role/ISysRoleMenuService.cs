﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysRoleMenuService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="menuIdList"></param>
    /// <returns></returns>
    Task DeleteRoleMenuListByMenuIdList(List<long> menuIdList);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roleId"></param>
    /// <returns></returns>
    Task DeleteRoleMenuListByRoleId(long roleId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roleIdList"></param>
    /// <returns></returns>
    Task<List<long>> GetRoleMenuIdList(List<long> roleIdList);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task GrantMenu(GrantRoleMenuInput input);
}