﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysRoleDataScopeService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="orgIdList"></param>
    /// <returns></returns>
    Task DeleteRoleDataScopeListByOrgIdList(List<long> orgIdList);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roleId"></param>
    /// <returns></returns>
    Task DeleteRoleDataScopeListByRoleId(long roleId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roleIdList"></param>
    /// <returns></returns>
    Task<List<long>> GetRoleDataScopeIdList(List<long> roleIdList);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task GrantDataScope(GrantRoleDataInput input);
}