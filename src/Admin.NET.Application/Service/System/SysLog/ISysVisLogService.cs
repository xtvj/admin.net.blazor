﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysVisLogService
{
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task ClearVisLog();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<VisLogOutput>> QueryVisLogPageList([FromQuery] VisLogPageInput input);
}