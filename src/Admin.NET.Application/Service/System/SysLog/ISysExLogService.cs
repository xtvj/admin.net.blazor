﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysExLogService
{
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task ClearExLog();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<ExLogOutput>> QueryExLogPageList([FromQuery] ExLogPageInput input);
}