﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface IMachineService
{
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<dynamic> GetMachineBaseInfo();

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<dynamic> GetMachineNetWorkInfo();

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<dynamic> GetMachineUseInfo();
}