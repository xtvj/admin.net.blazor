﻿using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ITokenService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<TokenOutput> GetTokenAsync(LoginInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<TokenOutput> RefreshTokenAsync(RefreshTokenInput input);
}