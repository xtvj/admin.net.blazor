﻿using Admin.NET.Contract.Constants;
using Admin.NET.Core.Options;
using Furion.Authorization;
using Furion.DataEncryption;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;

namespace Admin.NET.Application;

/// <summary>
/// 登录授权相关服务
/// </summary>
[ApiDescriptionSettings(Name = "Token", Order = 0)]
[Route("api")]
public class TokenService : ITokenService, IDynamicApiController, ITransient
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    private readonly IRepository<SysUser> _sysUserRep; // 用户表仓储
    private readonly ISysUserService _sysUserService; // 系统用户服务
    private readonly ISysEmpService _sysEmpService; // 系统员工服务

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sysUserRep"></param>
    /// <param name="httpContextAccessor"></param>
    /// <param name="sysUserService"></param>
    /// <param name="sysEmpService"></param>
    public TokenService(IRepository<SysUser> sysUserRep, 
        IHttpContextAccessor httpContextAccessor,
        ISysUserService sysUserService, 
        ISysEmpService sysEmpService)
    {
        _sysUserRep = sysUserRep;
        _httpContextAccessor = httpContextAccessor;
        _sysUserService = sysUserService;
        _sysEmpService = sysEmpService;
    }

    /// <summary>
    /// 用户登录
    /// </summary>
    /// <param name="input"></param>
    /// <remarks>默认用户名/密码：admin/admin</remarks>
    /// <returns></returns>
    [HttpPost("getToken")]
    [AllowAnonymous]
    public async Task<TokenOutput> GetTokenAsync([Required] LoginInput input)
    {
        // 获取加密后的密码
        var encryptPasswod = MD5Encryption.Encrypt(input.Password);

        // 判断用户名和密码是否正确 忽略全局过滤器
        var user = _sysUserRep
            .Where(u => u.Account == input.Account && u.Password == encryptPasswod && !u.IsDeleted, false, true)
            .FirstOrDefault() ?? throw Oops.Oh(ErrorCode.D1000);

        // 验证账号是否被冻结
        if (user.Status == CommonStatus.DISABLE)
            throw Oops.Oh(ErrorCode.D1017);

        // 员工信息
        var empInfo = await _sysEmpService.GetEmpInfo(user.Id);

        // 获取数据权限
        var dataScopes = (await _sysUserService.GetUserDataScopeIdList(user.Id)).ToJson();

        // 生成Token令牌
        //var accessToken = await _jwtBearerManager.CreateTokenAdmin(user);
        var accessToken = JWTEncryption.Encrypt(new Dictionary<string, object>
        {
            {ClaimConst.CLAINM_USERID, user.Id},
            {ClaimConst.TENANT_ID, user.TenantId},
            {ClaimConst.CLAINM_ACCOUNT, user.Account},
            {ClaimConst.CLAINM_NAME, user.Name},
            {ClaimConst.CLAINM_SUPERADMIN, user.AdminType},
            {ClaimConst.CLAINM_ORGID, empInfo.OrgId},
            {ClaimConst.CLAINM_ORGNAME, empInfo.OrgName},
            {ClaimConst.DATA_SCOPES, dataScopes}
        });

        // 设置Swagger自动登录
        _httpContextAccessor.HttpContext.SigninToSwagger(accessToken);

        // 生成刷新Token令牌
        var refreshToken =
            JWTEncryption.GenerateRefreshToken(accessToken, App.GetOptions<RefreshTokenSettingOptions>().ExpiredTime);

        // 设置刷新Token令牌
        _httpContextAccessor.HttpContext.Response.Headers["x-access-token"] = refreshToken;

        return new TokenOutput()
        {
            AccessToken = accessToken,
            RefreshToken = refreshToken
        };
    }

    /// <summary>
    /// 刷新token
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("refreshToken")]
    [AllowAnonymous]
    public async Task<TokenOutput> RefreshTokenAsync([Required] RefreshTokenInput input)
    {
        var result = new TokenOutput()
        {
            AccessToken = "invalid_token",
            RefreshToken = "invalid_token"
        };

        var httpContext = _httpContextAccessor.HttpContext;
        if (httpContext is null)
        {
            return result;
        }

        var accessTokenExpiredTime = App.GetOptions<JWTSettingsOptions>().ExpiredTime;
        var refreshTokenExpiredTime = App.GetOptions<RefreshTokenSettingOptions>().ExpiredTime;
        var newAccessToken = JWTEncryption.Exchange(input.AccessToken, input.RefreshToken, accessTokenExpiredTime, refreshTokenExpiredTime);

        if (string.IsNullOrWhiteSpace(newAccessToken))
        {
            return result;
        }

        var claims = JWTEncryption.ReadJwtToken(newAccessToken)?.Claims;
        if (claims is null)
        {
            return result;
        }

        var newRefreshToken = JWTEncryption.GenerateRefreshToken(newAccessToken, refreshTokenExpiredTime);
        var claimsIdentity = new ClaimsIdentity("AuthenticationTypes.Federation");
        claimsIdentity.AddClaims(claims);

        var principal = httpContext.User = new ClaimsPrincipal(claimsIdentity);
        await httpContext.SignInAsync(principal);

        string text2 = "access-token";
        string text3 = "x-access-token";
        string key = "Access-Control-Expose-Headers";

        httpContext.Response.Headers[text2] = newAccessToken;
        httpContext.Response.Headers[text3] = newRefreshToken;
        httpContext.Response.Headers.TryGetValue(key, out var value);
        httpContext.Response.Headers[key] = string.Join(',', StringValues.Concat(value, new StringValues(new string[2] { text2, text3 })).Distinct());

        result.AccessToken = newAccessToken;
        result.RefreshToken = newRefreshToken;

        return result;
    }
}