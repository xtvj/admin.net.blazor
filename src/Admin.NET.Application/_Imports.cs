﻿global using Admin.NET.Contract;
global using Admin.NET.Contract.System;
global using Admin.NET.Core;
global using Furion;
global using Furion.DatabaseAccessor;
global using Furion.DependencyInjection;
global using Microsoft.EntityFrameworkCore;
