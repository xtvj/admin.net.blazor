﻿using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Enjoy.Blazor;

public static partial class ServiceCollectionExtensions
{
    private static readonly Assembly Assembly = typeof(ServiceCollectionExtensions).Assembly;

    public static IMasaBlazorBuilder AddEnjoyBlazor(this IMasaBlazorBuilder builder)
    {
        var resources = new Dictionary<string, string>() {
            {"en-US","Enjoy.Blazor.Locales.en-US.json" },
            {"zh-CN","Enjoy.Blazor.Locales.zh-CN.json" }
        };

        foreach (var item in resources) 
        {
            using var stream = Assembly.GetManifestResourceStream(item.Value);
            if (stream is null)
            {
                continue;
            }

            using var reader = new StreamReader(stream);
            var content = reader.ReadToEnd();

            var locale = I18nReader.Read(content);

            builder.AddI18n((item.Key, locale));
        }

        return builder;
    }
}
