﻿namespace Enjoy.Blazor;

internal static class HandleEventExtensions
{
    public static EventCallback CreateEventCallback(this IHandleEvent? receiver, Action callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore(receiver, callback);
    }

    public static EventCallback CreateEventCallback(this IHandleEvent? receiver, Action<object> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore(receiver, callback);
    }

    public static EventCallback CreateEventCallback(this IHandleEvent? receiver, Func<Task> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore(receiver, callback);
    }

    public static EventCallback CreateEventCallback(this IHandleEvent? receiver, Func<object, Task> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore(receiver, callback);
    }

    public static EventCallback<TValue> CreateEventCallback<TValue>(this IHandleEvent? receiver, Action callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore<TValue>(receiver, callback);
    }

    public static EventCallback<TValue> CreateEventCallback<TValue>(this IHandleEvent? receiver, Action<TValue> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore<TValue>(receiver, callback);
    }

    public static EventCallback<TValue> CreateEventCallback<TValue>(this IHandleEvent? receiver, Func<Task> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore<TValue>(receiver, callback);
    }

    public static EventCallback<TValue> CreateEventCallback<TValue>(this IHandleEvent? receiver, Func<TValue, Task> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore<TValue>(receiver, callback);
    }

    private static EventCallback CreateCore(IHandleEvent receiver, MulticastDelegate callback)
        => new(receiver, callback);

    private static EventCallback<TValue> CreateCore<TValue>(IHandleEvent receiver, MulticastDelegate callback)
        => new(receiver, callback);
}
