﻿namespace Enjoy.Blazor;

/// <summary>
/// 查询扩展
/// </summary>
public static class QueryableExtension
{
    private static readonly Dictionary<Enum, string> LinqOperators = new ()
    {
        {Comparer.Equals , " == "},
        {Comparer.NotEquals , " != "},
        {Comparer.LessThan , " < "},
        {Comparer.LessThanOrEquals , " <= "},
        {Comparer.GreaterThan , " > "},
        {Comparer.GreaterThanOrEquals , " >= "},
        {Comparer.StartsWith , " StartsWith "},
        {Comparer.EndsWith , " EndsWith "},
        {Comparer.Contains , " Contains "},
        {Comparer.DoesNotContain , " DoesNotContain "},
        {Comparer.IsNull , " == "},
        {Comparer.IsEmpty , " == "},
        {Comparer.IsNotNull , " != "},
        {Comparer.IsNotEmpty , " != "},
        {Logical.And , " and "},
        {Logical.Or , " or "},
    };

    private static void AddWhereExpression(Filter filter, ref List<string> expressions, ref List<object?[]> values, ref int index)
    {
        if (filter.Children is not null)
        {
            var innerExpressions = new List<string>();

            foreach (var f in filter.Children)
            {
                AddWhereExpression(f, ref innerExpressions, ref values, ref index);
            }

            if (innerExpressions.Any())
            {
                expressions.Add("(" + string.Join(LinqOperators[filter.Logicaler], innerExpressions) + ")");
            }
        }
        else
        {
            if (string.IsNullOrWhiteSpace(filter.Key) || filter.Type is null)
            {
                return;
            }

            var property = $"it.{filter.Key}";

            switch (filter.Comparator)
            {
                case Comparer.IsNull:
                    expressions.Add($"{property} == null ");
                    break;
                case Comparer.IsNotNull:
                    expressions.Add($"{property} != null ");
                    break;
                case Comparer.IsEmpty:
                    expressions.Add($"{property} == string.Empty");
                    break;
                case Comparer.IsNotEmpty:
                    expressions.Add($"{property} != string.Empty");
                    break;
                default:
                    {
                        if (filter.Type == typeof(string))
                        {
                            var ignoreCase = filter.CaseInsensitive == true ? "OrdinalIgnoreCase" : "Ordinal";
                            switch (filter.Comparator)
                            {
                                case Comparer.Contains:
                                    expressions.Add($"np({property}).Contains(@{index},StringComparison.{ignoreCase})");
                                    break;
                                case Comparer.DoesNotContain:
                                    expressions.Add($"!np({property}).Contains(@{index},StringComparison.{ignoreCase})");
                                    break;
                                case Comparer.StartsWith:
                                    expressions.Add($"np({property}).StartsWith(@{index},StringComparison.{ignoreCase})");
                                    break;
                                case Comparer.EndsWith:
                                    expressions.Add($"np({property}).EndsWith(@{index},StringComparison.{ignoreCase})");
                                    break;
                                case Comparer.Equals:
                                    expressions.Add($"np({property}).Equals(@{index},StringComparison.{ignoreCase})");
                                    break;
                                case Comparer.NotEquals:
                                    expressions.Add($"!np({property}).Equals(@{index},StringComparison.{ignoreCase})");
                                    break;
                                default:
                                    throw new NotSupportedException();
                            }
                            values.Add(new object?[] { filter.Value });
                            index++;
                        }
                        else if (filter.Type.IsAssignableTo(typeof(IEnumerable)))
                        {
                            switch (filter.Comparator)
                            {
                                case Comparer.Contains:
                                    expressions.Add($@"(@{index}).Contains({property})");
                                    break;
                                case Comparer.DoesNotContain:
                                    expressions.Add($@"!(@{index}).Contains({property})");
                                    break;
                                default:
                                    throw new NotSupportedException();
                            }
                            values.Add(new object?[] { filter.Value });
                            index++;
                        }
                        else
                        {
                            expressions.Add($"{property} {LinqOperators[filter.Comparator]} @{index}");
                            values.Add(new object?[] { filter.Value });
                            index++;
                        }

                    };
                    break;
            }
        }
    }

    /// <summary>
    /// 筛选
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="filter"></param>
    /// <returns></returns>
    public static IQueryable<T> Where<T>(IQueryable<T> source, Filter? filter)
    {
        if (filter?.Children is null)
        {
            return source;
        }

        var index = 0;
        var expressions = new List<string>();
        var values = new List<object?[]>();

        foreach (var item in filter.Children)
        {
            AddWhereExpression(item, ref expressions, ref values, ref index);
        }

        return expressions.Count > 0
            ? source.Where(string.Join(LinqOperators[filter.Logicaler], expressions)
                , values.SelectMany(i => i.ToArray()).ToArray())
            : source;
    }

    /// <summary>
    /// 排序
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="filters"></param>
    /// <returns></returns>
    public static IQueryable<T> OrderBy<T>(IQueryable<T> source, IReadOnlyCollection<Sorter>? filters)
    {
        if (filters is null || filters.Count == 0)
        {
            return source;
        }

        return source.OrderBy(string.Join(",", filters.Select(x => $"it.{x.Key} {x.Direction}")));
    }
}
