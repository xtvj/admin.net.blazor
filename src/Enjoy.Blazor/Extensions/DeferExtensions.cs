﻿namespace Enjoy.Blazor;

internal static class DeferExtensions
{
    public static Task SafeChangStateAsync(this IRefreshState? defer)
    {
        if (defer is null)
        {
            return Task.CompletedTask;
        }

        return defer.RefreshState();
    }
}
