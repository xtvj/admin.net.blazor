﻿namespace Enjoy.Blazor;

public class AttributeConfig
{
    public AttributeConfig() { }

    public AttributeConfig(string? displayName, string? description, IReadOnlyDictionary<string, string?>? mappers)
    {
        DisplayName = displayName;
        Description = description;
        Mappers = mappers;
    }

    public string? DisplayName { get; }

    public string? Description { get; }

    public IReadOnlyDictionary<string, string?>? Mappers { get; }

    public string? this[string key]
    {
        get
        {
            if (Mappers is null)
            {
                return null;
            }

            if (Mappers.TryGetValue(key, out var value))
            {
                return value;
            }

            return null;
        }
    }
}