﻿namespace Enjoy.Blazor;

public record SelectItem<TValue>(string Name, TValue Value);
