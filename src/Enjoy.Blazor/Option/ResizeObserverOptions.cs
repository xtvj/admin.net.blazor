﻿using System.Text.Json.Serialization;

namespace Enjoy.Blazor;

public class ResizeObserverOptions
{
    [JsonPropertyName("borderBoxBlockSize")]
    public double? BorderBoxBlockSize { get; set; }

    [JsonPropertyName("borderBoxInlineSize")]
    public double? BorderBoxInlineSize { get; set; }

    [JsonPropertyName("contentBoxBlockSize")]
    public double? ContentBoxBlockSize { get; set; }

    [JsonPropertyName("contentBoxInlineSize")]
    public double? ContentBoxInlineSize { get; set; }
}
