﻿namespace Enjoy.Blazor;

/// <summary>
/// 排序描述对象
/// </summary>
public class Sorter
{
    public Sorter(string key, string direction)
    {
        Key = key;
        Direction = direction;
    }

    /// <summary>
    /// 获取 属性名称
    /// </summary>
    public string Key { get; set; }

    /// <summary>
    /// 获取 排序方向
    /// </summary>
    public string Direction { get; set; }
}
