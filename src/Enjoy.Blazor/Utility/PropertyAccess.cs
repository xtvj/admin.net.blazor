﻿namespace Enjoy.Blazor;

/// <summary>
/// 属性访问工具类
/// </summary>
internal static class PropertyAccess
{
    public static Type? GetPropertyType(Type? type, string? property)
    {
        if (string.IsNullOrWhiteSpace(property) || type is null)
        {
            return null;
        }

        if (property.Contains('.') == true)
        {
            var part = property.Split('.').FirstOrDefault();
            if (string.IsNullOrWhiteSpace(part))
            {
                return null;
            }

            return GetPropertyType(type.GetProperty(part)?.PropertyType, property.Replace($"{part}.", ""));
        }

        return type.GetProperty(property)?.PropertyType;
    }

    public static object? GetPropertyValue(object? value, string path)
    {
        if (value is null)
            return null;

        Type currentType = value.GetType();

        foreach (string propertyName in path.Split('.'))
        {
            var property = currentType.GetProperty(propertyName);
            if (property is not null)
            {
                if (value is not null)
                {
                    value = property.GetValue(value);
                }

                currentType = property.PropertyType;
            }
        }
        return value;
    }

    public static Func<TItem, TValue> Getter<TItem, TValue>(string propertyName)
    {
        var arg = Expression.Parameter(typeof(TItem));

        Expression body = arg;

        foreach (var member in propertyName.Split("."))
        {
            body = Expression.PropertyOrField(body, member);
        }

        body = Expression.Convert(body, typeof(TValue));

        return Expression.Lambda<Func<TItem, TValue>>(body, arg).Compile();
    }

    public static Action<TItem, TValue> Setter<TItem, TValue>(string propertyName)
    {
        var arg1 = Expression.Parameter(typeof(TItem));
        var arg2 = Expression.Parameter(typeof(TValue));

        Expression body = arg1;

        foreach (var member in propertyName.Split("."))
        {
            body = Expression.PropertyOrField(body, member);
        }
        return Expression.Lambda<Action<TItem, TValue>>(Expression.Assign(body, arg2), arg1, arg2).Compile();
    }
}
