﻿namespace Enjoy.Blazor;

public interface IFloating : IRefreshState
{
    Task HideAsync();
}