﻿using Masa.Blazor.Popup.Components;
using System.Text;

namespace Enjoy.Blazor;

/// <summary>
/// 浮动层 组件
/// </summary>
public sealed class EFloatingContent : PopupComponentBase, IFloating
{
    private string? _style;
    private ElementReference _reference;
    private JsDotNetInvoker? _invoker;
    private bool _clearAutoUpdateFlag;

    /// <summary>
    /// 获得/设置 浮动层显示状态，默认隐藏
    /// </summary>
    [Parameter]
    [NotNull]
    public FloatingOptions? Options { get; set; }

    protected override void OnInitialized()
    {
        base.OnInitialized();

        Options.OnClearAutoUpdate += OnClearAutoUpdateAsync;

        _style = new StringBuilder(Options.Style)
                .AddStyle("z-index: -1")
                .AddStyle("display: block")
                .AddStyle("visibility: hidden")
                .AddStyle("opacity: 0")
                .AddStyle("position:absolute")
                .AddStyle("transform: scale(0)")
                .AddStyle("transition: transform 0.2s ease, opacity 0.1s ease")
                .ToString();
    }

    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        if (Options.ShowOverlay)
        {
            builder.OpenComponent<MOverlay>(0);
            builder.AddAttribute(1, nameof(MOverlay.Value), true);
            builder.AddAttribute(2, nameof(MOverlay.ZIndex), Options.ZIndex - 1);
            builder.CloseComponent();
        }

        builder.OpenElement(3, "div");
        builder.AddAttribute(4, "id", Options.Id);
        builder.AddAttribute(5, "style", _style);
        builder.AddAttribute(6, "class", Options.Class);
        builder.AddAttribute(7, "role", "tooltip");


        if (Options.ChildContent is not null)
        {
            builder.AddContent(8, Options.ChildContent, this);
        }

        if (Options.UseArrow)
        {
            builder.OpenElement(9, "div");
            builder.AddAttribute(10, "class", Options.ArrowStyleClass);
            builder.AddAttribute(11, "style", $"position:absolute;{Options.ArrowClassStyle}");
            builder.AddAttribute(12, "data-element", "arrow");
            builder.CloseElement();
        }

        builder.AddElementReferenceCapture(13, x => _reference = x);
        builder.CloseElement();
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            _invoker = new JsDotNetInvoker(Js);
            await _invoker.FloatingElement(Options, _reference);
        }
        else if (_clearAutoUpdateFlag)
        {
            _clearAutoUpdateFlag = false;
            await _invoker!.ClearFloating(Options.Id);
        }
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (disposing)
        {
            _invoker?.Dispose();
            Options.OnClearAutoUpdate -= OnClearAutoUpdateAsync;
        }
    }

    private async Task OnClearAutoUpdateAsync()
    {
        if (Options.AutoUpdate)
        {
            _clearAutoUpdateFlag = true;
            await InvokeStateHasChangedAsync();
        }
    }

    Task IRefreshState.RefreshState() => InvokeStateHasChangedAsync();

    Task IFloating.HideAsync() => ClosePopupAsync(null);
}
