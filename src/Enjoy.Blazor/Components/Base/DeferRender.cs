﻿namespace Enjoy.Blazor;

/// <summary>
/// 延迟组件，用于增加渲染层级
/// </summary>
public class DeferRender : ComponentBase, IRefreshState
{
    [Parameter]
    public RenderFragment? ChildContent { get; init; }

    protected override void BuildRenderTree(RenderTreeBuilder builder) => builder.AddContent(0, ChildContent);

    public Task RefreshState() => InvokeAsync(StateHasChanged);
}
