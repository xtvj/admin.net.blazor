﻿namespace Enjoy.Blazor;

/// <summary>
/// 自我引用，用于内部手工渲染控制
/// </summary>
public sealed class ConveyOneSelf : ComponentBase, IRefreshState
{
    [Parameter]
    public RenderFragment<IRefreshState>? ChildContent { get; init; }

    protected override void BuildRenderTree(RenderTreeBuilder builder) => builder.AddContent(0, ChildContent, this);

    Task IRefreshState.RefreshState() => InvokeAsync(StateHasChanged);
}
