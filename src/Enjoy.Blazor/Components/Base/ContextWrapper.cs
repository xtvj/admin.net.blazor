﻿namespace Enjoy.Blazor;

/// <summary>
/// 类参数包装，方便参数引用
/// </summary>
public class ContextWrapper<TValue> : ComponentBase where TValue : class
{
    [Parameter]
    public RenderFragment<TValue>? ChildContent { get; init; }

    [Parameter]
    [NotNull]
    public TValue? Value { get; init; }

    protected override void BuildRenderTree(RenderTreeBuilder builder) => builder.AddContent(0, ChildContent, Value);
}
