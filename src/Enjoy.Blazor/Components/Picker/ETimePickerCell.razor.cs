﻿using Microsoft.AspNetCore.Components.Web;
using System.Text;

namespace Enjoy.Blazor;

/// <summary>
/// 时间选择滚轮单元组件
/// </summary>
public partial class ETimePickerCell : IDisposable
{
    /// <summary>
    /// 获得 当前样式名称
    /// </summary>
    private string? GetClassName(int index) => new StringBuilder("e-time-picker-spinner-item")
        .AddClass("prev", ViewMode switch
        {
            TimePickerCellViewMode.Hour => Value.Hours - 1 == index,
            TimePickerCellViewMode.Minute => Value.Minutes - 1 == index,
            _ => Value.Seconds - 1 == index
        })
        .AddClass("active", ViewMode switch
        {
            TimePickerCellViewMode.Hour => Value.Hours == index,
            TimePickerCellViewMode.Minute => Value.Minutes == index,
            _ => Value.Seconds == index
        })
        .AddClass("next", ViewMode switch
        {
            TimePickerCellViewMode.Hour => Value.Hours + 1 == index,
            TimePickerCellViewMode.Minute => Value.Minutes + 1 == index,
            _ => Value.Seconds + 1 == index
        })
        .ToString();

    /// <summary>
    /// 获得 滚轮单元数据区间
    /// </summary>
    private IEnumerable<int> Range => ViewMode switch
    {
        TimePickerCellViewMode.Hour => Enumerable.Range(0, 24),
        _ => Enumerable.Range(0, 60)
    };

    /// <summary>
    /// 获得 组件单元数据样式
    /// </summary>
    private string? StyleName => new StringBuilder()
        .AddStyle($"transform: translateY({CalcTranslateY()}px);")
        .ToString();

    /// <summary>
    /// 获得/设置 时间选择框视图模式
    /// </summary>
    [Parameter]
    public TimePickerCellViewMode ViewMode { get; set; }

    /// <summary>
    /// 获得/设置 组件值
    /// </summary>
    [Parameter]
    public TimeSpan Value { get; set; }

    /// <summary>
    /// 获得/设置 组件值变化时委托方法
    /// </summary>
    [Parameter]
    public EventCallback<TimeSpan> ValueChanged { get; set; }

    /// <summary>
    /// 上翻页按钮调用此方法
    /// </summary>
    private async Task HandleOnClickUp()
    {
        var ts = ViewMode switch
        {
            TimePickerCellViewMode.Hour => TimeSpan.FromHours(1),
            TimePickerCellViewMode.Minute => TimeSpan.FromMinutes(1),
            _ => TimeSpan.FromSeconds(1),
        };
        Value = Value.Subtract(ts);
        if (Value < TimeSpan.Zero)
        {
            Value = Value.Add(TimeSpan.FromHours(24));
        }
        if (ValueChanged.HasDelegate)
        {
            await ValueChanged.InvokeAsync(Value);
        }
    }

    /// <summary>
    /// 下翻页按钮调用此方法
    /// </summary>
    private async Task HandleOnClickDown()
    {
        var ts = ViewMode switch
        {
            TimePickerCellViewMode.Hour => TimeSpan.FromHours(1),
            TimePickerCellViewMode.Minute => TimeSpan.FromMinutes(1),
            _ => TimeSpan.FromSeconds(1)
        };
        Value = Value.Add(ts);
        if (Value.Days > 0)
        {
            Value = Value.Subtract(TimeSpan.FromDays(1));
        }

        if (ValueChanged.HasDelegate)
        {
            await ValueChanged.InvokeAsync(Value);
        }
    }

    /// <summary>
    /// 处理滚轮滚动
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    private Task HandleOnWheel(WheelEventArgs args)
    {
        if (args.DeltaY <= 0)
        {
            return HandleOnClickUp();
        }
        else
        {
            return HandleOnClickDown();
        }
    }

    private int CalcTranslateY()
    {
        return 0 - ViewMode switch
        {
            TimePickerCellViewMode.Hour => (Value.Hours) * 30,
            TimePickerCellViewMode.Minute => (Value.Minutes) * 30,
            _ => (Value.Seconds) * 30
        };
    }
}
