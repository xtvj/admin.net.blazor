﻿using System.ComponentModel;

namespace Enjoy.Blazor;

/// <summary>
/// 时间选择组件视图
/// </summary>
public enum TimePickerCellViewMode
{
    /// <summary>
    /// 小时
    /// </summary>
    [Description("小时")]
    Hour,

    /// <summary>
    /// 分钟
    /// </summary>
    [Description("分钟")]
    Minute,

    /// <summary>
    /// 秒
    /// </summary>
    [Description("秒")]
    Second,
}
