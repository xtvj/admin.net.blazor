﻿namespace Enjoy.Blazor;

/// <summary>
/// 自定义筛选配置列
/// </summary>
/// <typeparam name="TItem">自定义筛选对象类型</typeparam>
public class SettingColumn<TItem> : ComponentBase, IGridViewSetting where TItem : class, new()
{
    private readonly TItem _model;
    private GridViewFilterContext<TItem>? _filterContext;

    [CascadingParameter]
    [NotNull]
    IGridView? GridView { get; init; }

    [Parameter]
    public RenderFragment<GridViewFilterContext<TItem>>? FilterTemplate { get; init; }

    public SettingColumn()
    {
        _model = new TItem();
    }

    object IGridViewSetting.FilterModel => _model;

    RenderFragment? IGridViewSetting.FilterTemplate
    {
        get
        {
            if (_filterContext is null || FilterTemplate is null)
            {

                return null;
            }
            return FilterTemplate(_filterContext);
        }
    }

    protected override void OnInitialized()
    {
        if (GridView is null)
        {
            throw new ArgumentNullException(nameof(GridView));
        }
        _filterContext = new GridViewFilterContext<TItem>((IGridViewFilterContext)GridView, _model);
        GridView.UpdateSettings(this);
    }
}
 