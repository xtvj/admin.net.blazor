﻿using System.Text;

namespace Enjoy.Blazor;

/// <summary>
/// <see cref="IGridViewColumn{TItem}"/> 表格列组件基类
/// </summary>
/// <typeparam name="TItem">绑定数据对象</typeparam>
public abstract class ColumnBase<TItem> : ComponentBase, IGridViewColumn
{
    private const int MIN_COLUMN_WIDTH = 60;//限定最小列宽为 60px

    private readonly GridViewFilter _rowFilter;
    private readonly GridViewFilter _headFilter1;
    private readonly GridViewFilter _headFilter2;

    protected readonly List<IGridViewColumn> _children = new();
    protected readonly GridViewColumnSetting _props;
    protected IGridViewColumn? _parent;
    protected string? _key;
    protected string? _title;

    protected FieldIdentifier? _fieldIdentifier;
    protected string? _propertyName;
    protected Type? _propertyType;
    protected Type? _propertyValueType;
    protected bool _propertyNullable = true;

    protected string? _cellCssClass;
    protected string? _headerCellCssClass;
    protected string? _footerCellCssClass;
    protected string? _groupFooterCellCssClass;
    protected int? _width;
    protected int? _minWidth;
    protected int? _maxWidth;
    protected int _level;
    protected bool _tail;
    protected bool _fixed;
    protected bool _settable;
    protected bool _visible = true;
    protected bool _reorderable;
    protected bool _expandable;
    protected bool _resizable;
    protected bool _filterable;
    protected bool _groupable;
    protected bool _sortable;
    protected bool _caseInsensitive = true;

    protected DataTableHeaderAlign _headerAlign;
    protected DataTableHeaderAlign _align;
    protected Direction _direction;
    protected RenderFragment? _columns;
    protected RenderFragment? _template;
    protected RenderFragment? _editTemplate;
    protected RenderFragment? _headerTemplate;
    protected RenderFragment? _footerTemplate;
    protected RenderFragment<GridViewGroup>? _groupFooterTemplate;
    protected RenderFragment<IGridViewColumnFilter>? _filterTemplate;
    protected string? _formatString;
    protected Logical _logicaler = Logical.Or;
    protected Action<GridViewColumnSetting>? _settingProps;

    public string Identifier { get; }

    public bool Initialized { get; private set; }

    public bool IsString { get; private set; }

    public bool IsNumeric { get; private set; }

    public bool IsEnumerable { get; private set; }

    public bool IsDateRelated { get; private set; }

    public bool IsEnum { get; private set; }

    public bool IsBool { get; private set; }

    public IFilter RowFilter => _rowFilter;

    public IFilter HeadFilter1 => _headFilter1;

    public IFilter HeadFilter2 => _headFilter2;

    public string? HeaderFooterFixedStyle { get; private set; }

    public string? BodyDataCellFixedStyle { get; private set; }

    public ColumnBase()
    {
        _props = new();
        _rowFilter = new GridViewFilter() { Column = this };
        _headFilter1 = new GridViewFilter() { Column = this };
        _headFilter2 = new GridViewFilter() { Column = this };

        Identifier = Guid.NewGuid().ToString("N");
    }

    [CascadingParameter]
    [NotNull]
    public IGridView? GridView { get; init; }

    public IReadOnlyList<IGridViewColumn> Children => _children;

    protected override void OnInitialized()
    {
        Initialized = true;

        if (GridView is null)
        {
            throw new ArgumentNullException(nameof(GridView));
        }

        GridView.AddColumn(this);

        _parent?.AddColumn(this);

        //解析类型信息
        InitializePropertyType();

        //初始化筛选器
        _rowFilter.Reset();
        _headFilter1.Reset();
        _headFilter2.Reset();

        if (_settingProps is not null)
        {
            _settingProps(_props);

            _rowFilter.Logicaler = Logical.Or;
            _rowFilter.Comparator = _props.RowFilterComparator;
            _rowFilter.SetValue(_props.RowFilterValue);

            _headFilter1.Logicaler = _props.HeadFilterLogicaler;
            _headFilter1.Comparator = _props.HeadFilter1Comparator;
            _headFilter1.SetValue(_props.HeadFilter1Value);

            _headFilter2.Logicaler = _props.HeadFilterLogicaler;
            _headFilter2.Comparator = _props.HeadFilter2Comparator;
            _headFilter2.SetValue(_props.HeadFilter2Value);
        }
    }

    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        if (_columns is not null)
        {
            builder.OpenComponent<CascadingValue<IGridView>>(0);
            builder.AddAttribute(1, nameof(CascadingValue<IGridView>.Value), GridView);
            builder.AddAttribute(2, nameof(CascadingValue<IGridView>.ChildContent), new RenderFragment(x =>
            {
                x.OpenComponent<CascadingValue<IGridViewColumn>>(3);
                x.AddAttribute(4, nameof(CascadingValue<IGridViewColumn>.Value), this);
                x.AddAttribute(5, nameof(CascadingValue<IGridViewColumn>.ChildContent), _columns);
                x.CloseComponent();
            }));
            builder.CloseComponent();
        }
    }

    private void InitializePropertyType()
    {
        if (Initialized && _propertyType is not null)
        {
            var underlyingType = Nullable.GetUnderlyingType(_propertyType);
            _propertyValueType = underlyingType ?? _propertyType;
            var typeCode = Type.GetTypeCode(_propertyValueType);

            _propertyNullable = underlyingType is not null;
            if (_propertyValueType.IsEnum)
            {
                IsEnum = true;
            }
            else if (typeCode == TypeCode.SByte || typeCode == TypeCode.Byte || typeCode == TypeCode.Int16 ||
                typeCode == TypeCode.UInt16 || typeCode == TypeCode.Int32 || typeCode == TypeCode.UInt32 ||
                typeCode == TypeCode.Int64 || typeCode == TypeCode.UInt64 || typeCode == TypeCode.Single ||
                typeCode == TypeCode.Double || typeCode == TypeCode.Decimal)
            {
                IsNumeric = true;
            }
            else if (typeCode == TypeCode.String)
            {
                IsString = true;
            }
            else if (typeCode == TypeCode.Boolean)
            {
                IsBool = true;
            }
            else if (_propertyValueType == typeof(DateTime) || _propertyValueType == typeof(DateTimeOffset) ||
                _propertyValueType == typeof(DateOnly) || _propertyValueType == typeof(TimeOnly) ||
                _propertyValueType == typeof(DateTimeOffset))
            {
                IsDateRelated = true;
            }
            else if (_propertyValueType.IsGenericType && _propertyValueType.IsAssignableTo(typeof(IEnumerable)))
            {
                IsEnumerable = true;
            }
        }
    }

    private GridViewEnum<TEnum> GetI18nEnum<TEnum>(TEnum value) where TEnum : Enum
    {
        var config = value.ReadAttribute();
        var entry = new GridViewEnum<TEnum>()
        {
            Name = value.ToString(),
            Value = value,
            Symbol = config[Constants.Symbol]
        };

        if (_caseInsensitive)
        {
            entry.Symbol = entry.Symbol?.ToLowerInvariant();
        }

        if (Initialized)
        {
            var i18nValue = GridView.I18nT(config[Constants.I18nKey]);
            if (!string.IsNullOrWhiteSpace(i18nValue))
            {
                entry.Name = i18nValue;
            }
        }

        return entry;
    }

    public void AddColumn(IGridViewColumn column)
    {
        if (!_children.Contains(column))
        {
            _children.Add(column);
        }
    }

    public void RemoveColumn(IGridViewColumn column)
    {
        _children.Remove(column);
    }

    public void InsertColumn(IGridViewColumn column, IGridViewColumn target)
    {
        _children.Insert(_children.IndexOf(target), column);
    }

    public IGridViewColumn? GetParent() => _parent;

    public bool SetParent(IGridViewColumn? parent)
    {
        if (parent is null || parent.GetCategory() == ColumnType.Group)
        {
            _parent = parent;
            return true;
        }

        return false;
    }

    public string? GetKey() => _key;

    public virtual ColumnType GetCategory() => ColumnType.Data;

    public virtual string? GetStringValue(object item) => null;

    public int GetLevel()
    {
        int level = 0;
        var parent = _parent;
        while (parent is not null)
        {
            parent = parent.GetParent();
            level++;
        }

        return level;
    }

    public int GetColSpan(bool isDataCell = false)
    {
        if (isDataCell)
            return 1;

        var directChildColumns = Children.Where(c => c.GetVisible());

        if (_parent is null)
        {
            return _columns is null ? 1 : directChildColumns.Sum(c => c.GetColSpan());
        }

        return _columns is null ? 1 : directChildColumns.Count();
    }

    public int GetRowSpan(bool isDataCell = false)
    {
        if (isDataCell)
            return 1;

        if (_columns is null && _parent is not null)
        {
            var level = GetLevel();
            return level == GridView.StateOption.ColumnDeepestLevel ? 1 : level + 1;
        }

        return _columns is null && _parent is null ? GridView.StateOption.ColumnDeepestLevel + 1 : 1;
    }

    public string? GetTitle() => _title;

    public string? GetPropertyName() => _propertyName;

    public Type? GetPropertyType() => _propertyType;

    public bool GetSettable() => _settable;

    public bool GetTreeExpandable() => _expandable;

    public string? GetFormatString() => _formatString;

    public bool GetCaseInsensitive() => _caseInsensitive;

    public string GetDirectionAsString() => _direction switch
    {
        Direction.Asc => "asc",
        Direction.Desc => "desc",
        _ => string.Empty,
    };

    public DataTableHeaderAlign GetTextAlign() => _align;

    public DataTableHeaderAlign GetHeaderAlign() => _headerAlign;

    public string GetHeaderCellCssClass()
    {
        return new StringBuilder()
            .AddClass(_headerCellCssClass)
            .AddClass("sortable", GetSortable())
            .AddClass("frozen", IsFixed())
            .AddClass($"text-{_headerAlign.ToString().ToLower()}")
            .ToString();
    }

    public string? GetCellCssClass() => _cellCssClass;

    public string? GetFooterCellCssClass() => _footerCellCssClass;

    public string? GetGroupFooterCellCssClass() => _groupFooterCellCssClass;

    public virtual RenderFragment? InvokeTemplate(object data) => null;

    public virtual RenderFragment? InvokeEditTemplate(IGridViewRow row) => null;

    public virtual RenderFragment? InvokeGroupFooterTemplate(GridViewGroup? group)
    {
        if (_groupFooterTemplate is null || group is null)
        {
            return null;
        }

        return _groupFooterTemplate(group);
    }

    public RenderFragment InvokeHeaderTemplate()
    {
        if (_headerTemplate is not null)
        {
            return _headerTemplate;
        }

        return new RenderFragment(b => b.AddContent(0, _title));
    }

    public RenderFragment? GetColumns() => _columns;

    public RenderFragment? GetFooterTemplate() => _footerTemplate;

    public RenderFragment<IGridViewColumnFilter>? GetFilterTemplate() => _filterTemplate;

    public RenderFragment<GridViewGroup>? GetGroupFooterTemplate() => _groupFooterTemplate;

    public bool GetVisible() => _visible;

    public void SetVisible(bool value)
    {
        _visible = value;

        if (_parent is not null)
        {
            if (value)
            {
                _parent.SetVisible(true);
            }
            else if (!_parent.Children.Any(x => x.GetVisible()))
            {
                _parent.SetVisible(false);
            }
        }
    }

    public (bool Fixed, bool Tail) GetFixed() => (_fixed, _tail);

    public void SetFixed((bool Fixed, bool Tail) value) => (_fixed, _tail) = value;

    public bool GetFilterable() => _filterable;

    public void SetFilterable(bool value) => _filterable = value;

    public Logical GetLogicaler() => _logicaler;

    public void SetLogicaler(Logical value) => _logicaler = value;

    public bool GetReorderable() => _reorderable && GetCategory() == ColumnType.Data;

    public void SetReorderable(bool value) => _reorderable = value;

    public bool GetGroupable() => _groupable;

    public void SetGroupable(bool value) => _groupable = value;

    public bool GetSortable() => _sortable;

    public void SetSortable(bool value) => _sortable = value;

    public Direction GetDirection() => _direction;

    public void SetDirection(Direction value) => _direction = value;

    public bool GetResizable() => _resizable;

    public void SetResizable(bool value) => _resizable = value;

    public int GetWidth()
    {
        var width = _width ?? GridView.DefaultColumnWidth;

        if (_minWidth.HasValue)
        {
            width = Math.Max(width, _minWidth.Value);
        }

        if (_maxWidth.HasValue)
        {
            width = Math.Min(width, _maxWidth.Value);
        }

        if (width < MIN_COLUMN_WIDTH)
        {
            width = MIN_COLUMN_WIDTH;
        }

        return width;
    }

    public void SetWidth(int? value) => _width = value;

    public string? Fixed()
    {
        if (_parent is not null && _columns is not null)
        {
            return null;
        }

        if (_tail)
        {
            return "tail";
        }

        if (_fixed)
        {
            return "fixed";
        }

        return null;
    }

    public bool IsFixed() => (_fixed || _tail) && _parent is null && _columns is null;

    public bool IsHeadFixed() => _fixed && !_tail && _parent is null && _columns is null;

    public bool IsTailFixed() => _tail && _parent is null && _columns is null;

    public void UpdateFixedStyle(string cellStyle, string headerFooterStyle)
    {
        BodyDataCellFixedStyle = cellStyle;
        HeaderFooterFixedStyle = headerFooterStyle;
    }

    public IList<GridViewEnum<Logical>> GetLogicalers()
        => Enum.GetValues<Logical>().Select(x => GetI18nEnum(x)).ToArray();

    public IList<GridViewEnum<Comparer>> GetComparator()
    {
        var comparators = new List<GridViewEnum<Comparer>>
        {
            GetI18nEnum(Comparer.Empty)
        };

        if (IsEnum)
        {
            comparators.Add(GetI18nEnum(Comparer.Equals));
            comparators.Add(GetI18nEnum(Comparer.NotEquals));
        }
        else if (IsEnumerable)
        {
            comparators.Add(GetI18nEnum(Comparer.Contains));
            comparators.Add(GetI18nEnum(Comparer.DoesNotContain));
        }
        else if (IsString)
        {
            comparators.Add(GetI18nEnum(Comparer.Contains));
            comparators.Add(GetI18nEnum(Comparer.DoesNotContain));
            comparators.Add(GetI18nEnum(Comparer.StartsWith));
            comparators.Add(GetI18nEnum(Comparer.EndsWith));
            comparators.Add(GetI18nEnum(Comparer.IsEmpty));
            comparators.Add(GetI18nEnum(Comparer.IsNotEmpty));
            comparators.Add(GetI18nEnum(Comparer.Equals));
            comparators.Add(GetI18nEnum(Comparer.NotEquals));
        }
        else if (IsNumeric || IsDateRelated)
        {
            comparators.Add(GetI18nEnum(Comparer.Equals));
            comparators.Add(GetI18nEnum(Comparer.NotEquals));
            comparators.Add(GetI18nEnum(Comparer.GreaterThan));
            comparators.Add(GetI18nEnum(Comparer.GreaterThanOrEquals));
            comparators.Add(GetI18nEnum(Comparer.LessThan));
            comparators.Add(GetI18nEnum(Comparer.LessThanOrEquals));
        }
        else
        {
            comparators.Add(GetI18nEnum(Comparer.Equals));
            comparators.Add(GetI18nEnum(Comparer.NotEquals));
        }

        if (_propertyNullable)
        {
            comparators.Add(GetI18nEnum(Comparer.IsNull));
            comparators.Add(GetI18nEnum(Comparer.IsNotNull));
        }

        return comparators;
    }

    public IList<SelectItem<string>> GetPropertyEnums()
    {
        if (_propertyValueType is not null && (IsEnum || IsBool))
        {
            var list = new List<SelectItem<string>>();
            var type = IsEnum ? _propertyValueType : typeof(Boolean0);
            foreach (Enum item in Enum.GetValues(type))
            {
                var value = item.ToString();
                if (Initialized)
                {
                    var config = item.ReadAttribute();
                    var name = GridView.I18nT(config[Constants.I18nKey])
                        ?? config.Description
                        ?? config.DisplayName
                        ?? value;

                    list.Add(new(name, value));
                }
                else
                {
                    list.Add(new(value, value));
                }
            }
            return list;
        }

        return Array.Empty<SelectItem<string>>();
    }

    public Task ApplyFilterAsync(bool clear) => GridView.ApplyColumnFilter(this, clear);
}
