﻿namespace Enjoy.Blazor;

/// <summary>
/// 选择列组件，必须放置在 <see cref="EGridView{TItem}" />组件Column 模板中
/// </summary>
/// <typeparam name="TItem">数据对象类型</typeparam>
public class SelectColumn<TItem> : ColumnBase<TItem>
{
    /// <summary>
    /// 获取/设置 当前列列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? Width
    {
        get => _width;
        init => _width = value;
    }

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        _align = DataTableHeaderAlign.Center;
        _fixed = true;
        _tail = false;
    }

    public override ColumnType GetCategory() => ColumnType.Select;
}
