﻿namespace Enjoy.Blazor;

/// <summary>
/// 序号列组件，必须放置在 <see cref="EGridView{TItem}" />组件Column 模板中
/// </summary>
/// <typeparam name="TItem">数据对象类型</typeparam>
public class IndexColumn<TItem> : ColumnBase<TItem>
{
    /// <summary>
    /// 获取/设置 当前列标题
    /// </summary>
    [Parameter]
    public string? Title
    {
        get => _title;
        init => _title = value;
    }

    /// <summary>
    /// 获取/设置 当前列列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? Width
    {
        get => _width;
        init => _width = value;
    }

    /// <summary>
    /// 获取/设置 当前列最小列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? MinWidth
    {
        get => _minWidth;
        init => _minWidth = value;
    }

    /// <summary>
    /// 获取/设置 当前列最大列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? MaxWidth
    {
        get => _maxWidth;
        init => _maxWidth = value;
    }

    /// <summary>
    /// 获取/设置 是否支持调整列宽，默认为 false。
    /// </summary>
    [Parameter]
    public bool Resizable
    {
        get => _resizable;
        init => _resizable = value;
    }

    /// <summary>
    /// 获取/设置 表头单元格内容对齐方式
    /// </summary>
    [Parameter]
    public DataTableHeaderAlign HeaderAlign
    {
        get => _headerAlign;
        init => _headerAlign = value;
    }

    /// <summary>
    /// 获取/设置 内容对齐方式
    /// </summary>
    [Parameter]
    public DataTableHeaderAlign Align
    {
        get => _align;
        init => _align = value;
    }

    public IndexColumn() : base()
    {
        _align = DataTableHeaderAlign.Center;
        _headerAlign = DataTableHeaderAlign.Center;
    }

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();
        _fixed = true;
        _tail = false;
    }

    public override ColumnType GetCategory() => ColumnType.Index;
}
