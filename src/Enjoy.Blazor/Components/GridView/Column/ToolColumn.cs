﻿namespace Enjoy.Blazor;

/// <summary>
/// 工具列组件，必须放置在 <see cref="EGridView{TItem}" />组件Column 模板中
/// </summary>
/// <typeparam name="TItem">数据对象类型</typeparam>
public class ToolColumn<TItem> : ColumnBase<TItem>
{
    /// <summary>
    /// 获取/设置 当前列标题
    /// </summary>
    [Parameter]
    public string? Title
    {
        get => _title;
        init => _title = value;
    }

    /// <summary>
    /// 获取/设置 是否支持冻结，默认为空不冻结。
    /// </summary>
    [Parameter]
    public bool Frozen
    {
        get => _fixed;
        init => _fixed = value;
    }

    /// <summary>
    /// 获取/设置 指示是否尾部（与<see cref="Frozen"/>配合指尾部冻结）
    /// </summary>
    [Parameter]
    public bool Tail
    {
        get => _tail;
        init => _tail = value;
    }

    /// <summary>
    /// 获取/设置 当前列列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? Width
    {
        get => _width;
        init => _width = value;
    }

    /// <summary>
    /// 获取/设置 当前列最小列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? MinWidth
    {
        get => _minWidth;
        init => _minWidth = value;
    }

    /// <summary>
    /// 获取/设置 当前列最大列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? MaxWidth
    {
        get => _maxWidth;
        init => _maxWidth = value;
    }

    /// <summary>
    /// 获取/设置 表头单元格内容对齐方式
    /// </summary>
    [Parameter]
    public DataTableHeaderAlign HeaderAlign
    {
        get => _headerAlign;
        init => _headerAlign = value;
    }

    /// <summary>
    /// 获取/设置 内容对齐方式
    /// </summary>
    [Parameter]
    public DataTableHeaderAlign Align
    {
        get => _align;
        init => _align = value;
    }

    /// <summary>
    /// 获取/设置 内容模板，Context为 <see cref="{TItem}"/>
    /// </summary>
    [Parameter]
    public RenderFragment<NotNullRef<TItem>>? Template { get; init; }

    /// <summary>
    /// 获取/设置 是否支持调整列宽，默认为 false。
    /// </summary>
    [Parameter]
    public bool Resizable
    {
        get => _resizable;
        init => _resizable = value;
    }

    public ToolColumn() : base()
    {
        _fixed = true;
        _tail = true;
        _headerAlign = DataTableHeaderAlign.Center;
        _align = DataTableHeaderAlign.Center;
    }

    public override ColumnType GetCategory() => ColumnType.Tool;

    public override RenderFragment? InvokeTemplate(object data)
    {
        if (Template is null)
        {
            return null;
        }

        return Template(new((TItem)data));
    }
}
