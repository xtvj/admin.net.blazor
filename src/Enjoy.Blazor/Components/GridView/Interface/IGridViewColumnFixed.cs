﻿namespace Enjoy.Blazor;

public interface IGridViewColumnFixed
{
    string? HeaderFooterFixedStyle { get; }

    string? BodyDataCellFixedStyle { get; }

    (bool Fixed, bool Tail) GetFixed();

    void SetFixed((bool Fixed, bool Tail) value);

    string? Fixed();

    bool IsFixed();

    bool IsHeadFixed();

    bool IsTailFixed();

    void UpdateFixedStyle(string cellStyle, string headerFooterStyle);
}