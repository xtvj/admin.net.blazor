﻿namespace Enjoy.Blazor;

public interface IGridViewEditor : IGridView
{
    IReadOnlyCollection<string> Permissions { get; }

    Task HandleOnSaveInlineEdit(IGridViewRow row);

    Task HandleOnActionClick(MouseEventArgs e, MButton button, IGridViewRow? row);
}