﻿namespace Enjoy.Blazor;

public interface IGridViewSetting : IHandleEvent
{
    object FilterModel { get; }

    RenderFragment? FilterTemplate { get; }
}