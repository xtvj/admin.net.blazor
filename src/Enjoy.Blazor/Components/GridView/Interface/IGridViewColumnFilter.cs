﻿namespace Enjoy.Blazor;

public interface IGridViewColumnFilter
{
    IFilter RowFilter { get; }

    IFilter HeadFilter1 { get; }

    IFilter HeadFilter2 { get; }

    bool GetFilterable();

    Logical GetLogicaler();

    IList<GridViewEnum<Logical>> GetLogicalers();

    IList<GridViewEnum<Comparer>> GetComparator();

    Task ApplyFilterAsync(bool clear);
}