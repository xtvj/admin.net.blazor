﻿namespace Enjoy.Blazor;

/// <summary>
/// 自定义筛选
/// </summary>
public interface IGridViewFilterContext
{
    /// <summary>
    /// 重置筛选表单
    /// </summary>
    EventCallback Reset();

    /// <summary>
    /// 应用筛选
    /// </summary>
    EventCallback ApplyFilter();

    /// <summary>
    /// 清空筛选
    /// </summary>
    EventCallback ClearFilter();
}

/// <summary>
/// 自定义筛选
/// </summary>
/// <typeparam name="TItem"></typeparam>
public interface IGridViewFilterContext<TItem> : IGridViewFilterContext
{
    /// <summary>
    /// <b>可选：</b> 动态筛选对象
    /// 如果不想被 <b>GridView</b> 绑定的对象类型限制，可以使用索引器动态创建筛选值对象
    /// 会自动匹配 <b>DisplayName</b> 相同的属性以进行筛选操作
    /// </summary>
    IFilter this[string Key] { get; }

    TItem Value { get; }
}