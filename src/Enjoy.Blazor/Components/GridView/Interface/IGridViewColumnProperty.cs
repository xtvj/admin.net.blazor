﻿namespace Enjoy.Blazor;

public interface IGridViewColumnProperty
{
    bool IsString { get; }

    bool IsNumeric { get; }

    bool IsEnumerable { get; }

    bool IsDateRelated { get; }

    bool IsEnum { get; }

    bool IsBool { get; }

    string? GetPropertyName();

    Type? GetPropertyType();
}