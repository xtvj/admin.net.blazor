﻿namespace Enjoy.Blazor;

public interface IGridView
{
    bool IsDark { get; }

    bool IsMobile { get; }

    bool IsDense { get; }

    string? I18nT(string? key);

    IReadOnlyList<IGridViewColumn> LeafColumns { get; }

    void AddColumn(IGridViewColumn column);

    void UpdateSettings(IGridViewSetting setting);

    CultureInfo DefaultCulture { get; }

    int DefaultColumnWidth { get; }

    string DefaultDateFormat { get; }

    string DefaultTimeFormat { get; }

    Logical DefaultLogicaler { get; }

    GridViewStateOption StateOption { get; }

    Task RefreshAsync(bool reset);

    Task ApplyColumnFilter(IGridViewColumn filter, bool clear = false);
}