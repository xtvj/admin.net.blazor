﻿namespace Enjoy.Blazor;

public class EGridViewCustomFilter : MForm, IRefreshState
{
    Task IRefreshState.RefreshState() => InvokeStateHasChangedAsync();
}
