﻿namespace Enjoy.Blazor;

/// <summary>
/// 单元格渲染回调事件参数 <see cref="EGridView{TItem}.OnBeforeCellRender" />
/// </summary>
/// <typeparam name="TItem"></typeparam>
public class GridViewCellRenderEventArgs<TItem> : GridViewRowRenderEventArgs<TItem>
{
    public GridViewCellRenderEventArgs(TItem data, IGridViewColumn column) : base(data) => Column = column;

    public IGridViewColumn Column { get; private set; }

    public int? Colspan { get; set; }

    public int? Rowspan { get; set; }
}
