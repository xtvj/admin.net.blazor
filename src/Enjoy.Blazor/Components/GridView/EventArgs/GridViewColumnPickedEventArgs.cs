﻿namespace Enjoy.Blazor;

/// <summary>
/// 列显示变化回调事件参数 <see cref="EGridView{TItem}.OnColumnPicked" />
/// </summary>
/// <typeparam name="TItem"></typeparam>
public class GridViewColumnPickedEventArgs
{
    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="columns">所选列</param>
    public GridViewColumnPickedEventArgs(IEnumerable<IGridViewColumn> columns)
    {
        Columns = columns;
    }

    /// <summary>
    /// 获取 所选列
    /// </summary>
    public IEnumerable<IGridViewColumn> Columns { get; private set; }
}
