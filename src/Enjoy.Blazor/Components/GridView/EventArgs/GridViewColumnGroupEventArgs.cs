﻿namespace Enjoy.Blazor;

/// <summary>
/// 分组回调事件参数
/// </summary>
/// <typeparam name="TItem"></typeparam>
public class GridViewColumnGroupEventArgs
{
    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="column">当前列</param>
    /// <param name="descriptor">分组的描述信息</param>
    public GridViewColumnGroupEventArgs(IGridViewColumn column)
    {
        Column = column;
    }

    /// <summary>
    /// 获取 当前列
    /// </summary>
    public IGridViewColumn Column { get; private set; }
}
