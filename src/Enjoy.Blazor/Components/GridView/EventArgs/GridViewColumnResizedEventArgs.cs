﻿namespace Enjoy.Blazor;

/// <summary>
/// 列宽调整回调事件参数
/// </summary>
/// <typeparam name="TItem"></typeparam>
public class GridViewColumnResizedEventArgs
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="column"></param>
    /// <param name="width"></param>
    public GridViewColumnResizedEventArgs(IGridViewColumn column, double width)
    {
        Column = column;
        Width = width;
    }

    /// <summary>
    /// 获取 当前列
    /// </summary>
    public IGridViewColumn Column { get; private set; }

    /// <summary>
    /// 获取 当前列调整后的列宽
    /// </summary>
    public double Width { get; private set; }
}
