﻿namespace Enjoy.Blazor;

public class GridViewButtonMouseEventArgs : MouseEventArgs
{
    public GridViewButtonMouseEventArgs(MouseEventArgs args, IEnumerable? selectedItems, object? rowItem) : base()
    {
        Type = args.Type;
        AltKey = args.AltKey;
        Button = args.Button;
        Buttons = args.Buttons;
        ClientX = args.ClientX;
        ClientY = args.ClientY;
        CtrlKey = args.CtrlKey;
        Detail = args.Detail;
        MetaKey = args.MetaKey;
        OffsetX = args.OffsetX;
        OffsetY = args.OffsetY;
        ScreenX = args.ScreenX;
        ScreenY = args.ScreenY;
        ShiftKey = args.ShiftKey;

        SelectedItems = selectedItems;
        RowItem = rowItem;
    }

    public IEnumerable? SelectedItems { get; }

    public object? RowItem { get; }
}
