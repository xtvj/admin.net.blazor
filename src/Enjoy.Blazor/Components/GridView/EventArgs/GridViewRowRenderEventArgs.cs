﻿namespace Enjoy.Blazor;

/// <summary>
/// 行渲染回调事件参数
/// </summary>
public class GridViewRowRenderEventArgs<TItem>
{
    internal GridViewRowRenderEventArgs(TItem item)
    {
        Data = item ?? throw new NullReferenceException(nameof(Data));
    }

    /// <summary>
    /// 获取 当前行表示的数据项。
    /// </summary>
    [NotNull]
    public TItem Data { get; }

    /// <summary>
    /// 获取/设置 指示此行是否可展开
    /// </summary>
    public bool Expandable { get; set; }

    /// <summary>
    /// 获取/设置 此行样式
    /// </summary>
    public string? CssClass { get; set; }

    /// <summary>
    /// 获取/设置 此行式样
    /// </summary>
    public string? CssStyle { get; set; }
}
