﻿using Microsoft.Extensions.Logging;

namespace Enjoy.Blazor;

/// <summary>
/// EGridView 表格视图组件
/// </summary>
/// <typeparam name="TItem"></typeparam>
[CascadingTypeParameter(nameof(TItem))]
public partial class EGridView<TItem>
{
    private bool _firstRender = false;
    private readonly GridViewSetting _props;

    private readonly TItem _filterModel = new();
    private readonly Dictionary<string, Filter> _filters = new();
    private readonly Filter _filter;

    private IJSObjectReference _module = default!;
    private DotNetObjectReference<EGridView<TItem>>? _interop;
    private JsDotNetInvoker? _jsDotNetInvoker;
    private Virtualize<TItem>? _virtualize;
    private Virtualize<GroupResult>? _groupVirtualize;
    private EGridViewAdd<TItem>? _addSection;

    private ElementReference _element;
    private IGridViewSetting? _setting;
    private EGridViewCustomFilter? _filterForm;
    private IRefreshState? _editForm;
    private IRefreshState? _loadingSection;
    private IRefreshState? _contentSection;
    private IRefreshState? _settingSection;
    private IRefreshState? _topbarSection;
    private IRefreshState? _bottomSection;
    private IRefreshState? _tableSection;

    private bool _shouldRender = true;
    private bool _isMobileBreaked;
    private bool _processDataFlag;
    private bool _updateValueFlag;

    private List<TItem>? _value;
    private IEnumerable<TItem>? _data;
    private string? _filterKeywords;
    private SelectedState _allSelectedState;
    private Func<TItem, IEnumerable<TItem>>? _childrenGetter;

    public GridViewPagerOption PagerOption { get; }
    public GridViewStateOption StateOption { get; }

    public EGridView()
    {
        _filters = new();
        _filter = new();
        _props = new();

        PagerOption = new();
        StateOption = new();
    }

    public bool IsDark => Dark || (!Light && CascadingIsDark);

    public bool IsMobile => StateOption.IsMobileMode || _isMobileBreaked;

    public bool IsDense => StateOption.IsDenseStyle || Dense;

    private bool AllowPaging => AllowBottomPaging || AllowTopPaging;

    protected override bool ShouldRender() => _shouldRender;

    protected override void OnInitialized()
    {
        base.OnInitialized();

        //不常用或细节配置参数初始化
        SettingProps?.Invoke(_props);

        StateOption.IsDenseStyle = Dense;
        StateOption.IsMobileMode = _props.Mobile;
        StateOption.IsCustomFilterExpanded = _props.ExpandedCustomFilter;
        StateOption.HasExpandTemplate = ExpandTemplate is not null;
        PagerOption.ItemsPerPage = Math.Max(10, ItemsPerPage);
        _filter.Logicaler = DefaultLogicaler;

        // 判断子节点是不是 IEnumerable<TItem>类型
        if (!string.IsNullOrWhiteSpace(ChildrenProperty))
        {
            var property = typeof(TItem).GetProperty(ChildrenProperty);
            if (property is not null)
            {
                var valueType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                if (valueType.IsAssignableTo(typeof(IEnumerable<TItem>)))
                {
                    _childrenGetter = PropertyAccess.Getter<TItem, IEnumerable<TItem>>(ChildrenProperty);
                }
            }
        }

        StateOption.IsAccessControl = OnLoadPermissionAsync is not null;
        StateOption.IsTreeGridView = TreeNodeConverter is not null || _childrenGetter is not null;
        StateOption.IsRowClickable = OnRowClick is not null;
        StateOption.IsRowDoubleClickable = OnRowDoubleClick is not null;
    }

    public override async Task SetParametersAsync(ParameterView parameters)
    {
        await base.SetParametersAsync(parameters);
        await _editForm.SafeChangStateAsync();
        await _addSection.SafeChangStateAsync();
        await _filterForm.SafeChangStateAsync();
    }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        //OnAfterRenderAsync 之后 Data 参数若发生变化
        if (_firstRender)
        {
            if (_processDataFlag)
            {
                _processDataFlag = false;
                await RefreshAsync(false);
            }
            else if (_updateValueFlag)
            {
                _updateValueFlag = false;
                await OnSelectedValueChange();
            }
        }
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        Logger.LogInformation("GridView:OnAfterRenderAsync.");
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            _interop = DotNetObjectReference.Create(this);
            _module = await Js.InvokeAsync<IJSObjectReference>("import", "./_content/Enjoy.Blazor/module/GridView.js");
            _isMobileBreaked = await _module.InvokeAsync<bool>("mobileBreak", _element, _interop, MobileBreakWidth);

            await ReloadDataAsync(false);
            await LoadPermissionCallback();

            //初始化可见列
            CaptureColumns();

            //下次渲染执行
            NextTick(FirstRenderComplete);

            //第一次数据加载完，才标记完成初载
            _firstRender = true;
            StateHasChanged();
        }
    }

    async ValueTask IAsyncDisposable.DisposeAsync()
    {
        _allColumns.Clear();
        _orphanColumns.Clear();
        _leafColumns.Clear();
        _cachedSortedColumns.Clear();
        _cachedGroupedColumns.Clear();
        _jsDotNetInvoker?.Dispose();
        _interop?.Dispose();
        if (_module is not null)
        {
            await _module.DisposeAsync();
        }
        GC.SuppressFinalize(this);
    }

    string? IGridView.I18nT(string? key) => I18n.T(key, false);

    private async Task FirstRenderComplete()
    {
        _shouldRender = false;
        OnAfterRenderComplete?.Invoke(new GridViewRenderEventArgs());
        await _module.InvokeVoidAsync("bindCellClick", _element, _interop);
        await HideLoadingAsync();
    }

    [JSInvokable]
    public async ValueTask Breaked(bool breaked)
    {
        _isMobileBreaked = breaked;

        //在移动模式下不动态切换
        if (_props.Mobile)
        {
            return;
        }

        await InvokeAsync(() =>
        {
            _shouldRender = true;
            StateHasChanged();
        });
    }

    [JSInvokable]
    public ValueTask CellClick(string? rowId, string? columnId)
    {
        Logger.LogInformation($"GridView:CellClick:rowId:{rowId},columnId:{columnId}.");
        return ValueTask.CompletedTask;
    }
}
  