﻿using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    TItem IGridViewFilterContext<TItem>.Value => _filterModel;

    IFilter IGridViewFilterContext<TItem>.this[string key]
    {
        get
        {
            if (_filters.TryGetValue(key, out var value))
            {
                return value;
            }
            else
            {
                var result = new Filter()
                {
                    Key = key,
                    Logicaler = Logical.And,
                    Comparator = Comparer.Equals
                };
                _filters.Add(key, result);

                return result;
            }
        }
    }

    EventCallback IGridViewFilterContext.Reset()
    {
        if (_filterForm is null)
        {
            return EventCallback.Empty;
        }

        return _filterForm.CreateEventCallback(_filterForm.Reset);
    }

    EventCallback IGridViewFilterContext.ApplyFilter()
    {
        if (_filterForm is null)
        {
            return EventCallback.Empty;
        }

        return _filterForm.CreateEventCallback(() => RefreshAsync(true));
    }

    EventCallback IGridViewFilterContext.ClearFilter()
    {
        if (_filterForm is null)
        {
            return EventCallback.Empty;
        }

        return _filterForm.CreateEventCallback(() =>
        {
            _filterForm.Reset();
            return RefreshAsync(true);
        });
    }

    private Filter? GetFilters()
    {
        var hasKeywords = AllowKeywordsFiltering && !string.IsNullOrWhiteSpace(_filterKeywords);
        var rootfilter = new Filter() { Logicaler = Logical.Or };

        //合并 首行、表头筛选、关键字筛选
        if (AllowHeadFiltering || AllowRowFiltering || AllowKeywordsFiltering)
        {
            var rowFilter = new Filter(DefaultLogicaler);
            var headFilter = new Filter(DefaultLogicaler);
            var keywordsFilter = new Filter(DefaultLogicaler);

            foreach (var column in _leafColumns)
            {
                if (column.GetFilterable())
                {
                    //行内筛选
                    if (AllowRowFiltering && column.RowFilter.IsActive())
                    {
                        rowFilter.AddChild(column.RowFilter.Clone());
                    }

                    //表头筛选
                    if (AllowHeadFiltering)
                    {
                        var active1 = column.HeadFilter1.IsActive();
                        var active2 = column.HeadFilter2.IsActive();
                        if (active1 || active2)
                        {
                            if (active1 && !active2)
                            {
                                headFilter.AddChild(column.HeadFilter1.Clone());
                            }
                            else if (!active1 && active2)
                            {
                                headFilter.AddChild(column.HeadFilter2.Clone());
                            }
                            else
                            {
                                var group = new Filter(column.GetLogicaler());
                                group.AddChild(column.HeadFilter1.Clone());
                                group.AddChild(column.HeadFilter2.Clone());
                                headFilter.AddChild(group);
                            }
                        }
                    }

                    //关键字筛选
                    if (hasKeywords && column.IsString)
                    {
                        keywordsFilter.AddChild(new Filter()
                        {
                            Logicaler = Logical.Or,
                            Key = column.GetPropertyName(),
                            Type = column.GetPropertyType(),
                            CaseInsensitive = column.GetCaseInsensitive(),
                            Value = _filterKeywords,
                            Comparator = Comparer.Contains,
                        });
                    }
                }
            }

            if (AllowRowFiltering && rowFilter.Children?.Count > 0)
            {
                rootfilter.AddChild(rowFilter);
            }

            if (AllowHeadFiltering && headFilter.Children?.Count > 0)
            {
                rootfilter.AddChild(headFilter);
            }

            if(hasKeywords && keywordsFilter.Children?.Count > 0)
            {
                rootfilter.AddChild(keywordsFilter);
            }
        }

        //合并 自定义筛选
        if (AllowCustomFiltering)
        {
            var customFilter = new Filter() { Logicaler = Logical.Or };

            foreach (var filter in _filters.Values)
            {
                if (filter.IsActive() && _leafColumns.Exists(x => 
                    string.Equals(x.GetPropertyName(), filter.Key, StringComparison.OrdinalIgnoreCase)))
                {
                    customFilter.AddChild(filter.Clone());
                }
            }

            if (customFilter.Children?.Count > 0)
            {
                rootfilter.AddChild(customFilter);
            }
        }

        //合并 动态筛选
        if (AllowDynamicFiltering)
        {
            rootfilter.AddChild(BuildDynamicFilter(_filter));
        }

        Logger.LogInformation(JsonSerializer.Serialize(rootfilter));

        return rootfilter.Children is null ? null : rootfilter;

        static Filter? BuildDynamicFilter(Filter filter)
        {
            if (filter.Children is null || filter.Children.Count == 0)
            {
                return null;
            }

            //跳过无效的单层节点
            if (filter.Children.Count == 1)
            {
                var node = filter.Children.First();
                if (node.Children?.Count > 0)
                {
                    return BuildDynamicFilter(node);
                }
                else if (node is GridViewFilter)
                {
                    var group = new Filter() { Logicaler = filter.Logicaler };
                    group.AddChild(node.Clone());
                    return group;
                }
            }
            else
            {
                var group = new Filter() { Logicaler = filter.Logicaler };
                foreach (var item in filter.Children)
                {
                    if (item.Children?.Count > 0)
                    {
                        group.AddChild(BuildDynamicFilter(item));
                    }
                    else if (item is GridViewFilter)
                    {
                        group.AddChild(item.Clone());
                    }
                }

                if (group.Children is not null)
                {
                    return group;
                }
            }

            return null;
        }
    }

    /// <summary>
    /// 处理列上的筛选（行间筛选和表头筛选）操作
    /// </summary>
    /// <param name="column">筛选列</param>
    /// <param name="clear">是否清空筛选</param>
    /// <returns></returns>
    public async Task ApplyColumnFilter(IGridViewColumn column, bool clear = false)
    {
        if (!AllowHeadFiltering && !AllowRowFiltering && !column.GetFilterable())
        {
            return;
        }

        if (clear)
        {
            column.RowFilter.Reset();
            column.HeadFilter1.Reset();
            column.HeadFilter2.Reset();

            await RefreshAsync(false);
        }
        else
        {
            await RefreshAsync(true);
        }
    }

    /// <summary>
    /// 处理列筛选（行间筛选和表头筛选）操作
    /// </summary>
    /// <param name="keyOrProperty">列名或自定义标识</param>
    /// <param name="action">筛选操作符</param>
    /// <param name="value">筛选值</param>
    /// <returns></returns>
    public async Task ApplyColumnFilter(string keyOrProperty, Comparer action, string? value)
    {
        if (!AllowHeadFiltering && !AllowRowFiltering || string.IsNullOrWhiteSpace(keyOrProperty))
        {
            return;
        }

        var columns = _leafColumns.Where(x => x.GetFilterable());
        var column = columns.FirstOrDefault(c => c.GetKey() == keyOrProperty)
            ?? columns.FirstOrDefault(c => c.GetPropertyName() == keyOrProperty);

        if (column is not null)
        {
            column.RowFilter.SetValue(value);
            column.RowFilter.Comparator = action;

            column.HeadFilter1.Reset();
            column.HeadFilter2.Reset();

            await RefreshAsync(true);
        }
    }

    private async void HandleOnApplyDynamicFilter(IPopupHandle handle)
    {
        await handle.OnClose.InvokeAsync();
        await RefreshAsync(true);
    }

    private async void HandleOnClearDynamicFilter(IPopupHandle handle)
    {
        _filter.ClearChildren();
        await handle.OnClose.InvokeAsync();
        await RefreshAsync(true);
    }
}
