﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private float GetVirtualizeItemSize() => IsDense ? 36 : 48;

    private ValueTask<ItemsProviderResult<TItem>> LoadItems(ItemsProviderRequest request)
    {
        var top = request.Count > 0 ? request.Count : PagerOption.ItemsPerPage;
        var result = new ItemsProviderResult<TItem>(_view.Skip(request.StartIndex).Take(top), _view.Count);
        return ValueTask.FromResult(result);
    }

    private ValueTask<ItemsProviderResult<GroupResult>> LoadGroups(ItemsProviderRequest request)
    {
        var top = request.Count > 0 ? request.Count : PagerOption.ItemsPerPage;

        //虚拟滚动的分组只能针对当前分页的数据
        var order = _cachedGroupedColumns.Where(c => c.GetDirection() != Direction.None);
        IEnumerable<TItem> query = order.Any()
            ? _view.AsQueryable().OrderBy(string.Join(',', order.Select(c => $"np(it.{c.GetPropertyName()}) {c.GetDirectionAsString()}")))
            : _view;

        var view = query.GroupByMany(_cachedGroupedColumns.Select(c => $"np(it.{c.GetPropertyName()})").ToArray()).ToList();
        var result = new ItemsProviderResult<GroupResult>(view.Skip(request.StartIndex).Take(top), view.Count);

        _groupedView = view;

        return ValueTask.FromResult(result);
    }
}
