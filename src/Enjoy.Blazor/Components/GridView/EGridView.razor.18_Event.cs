﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private static EventCallback CreateEventCallback(IHandleEvent? receiver, Action callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore(receiver, callback);
    }

    private static EventCallback CreateEventCallback(IHandleEvent? receiver, Action<object> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore(receiver, callback);
    }

    private static EventCallback CreateEventCallback(IHandleEvent? receiver, Func<Task> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore(receiver, callback);
    }

    private static EventCallback CreateEventCallback(IHandleEvent? receiver, Func<object, Task> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore(receiver, callback);
    }

    private static EventCallback<TValue> CreateEventCallback<TValue>(IHandleEvent? receiver, Action callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore<TValue>(receiver, callback);
    }

    private static EventCallback<TValue> CreateEventCallback<TValue>(IHandleEvent? receiver, Action<TValue> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore<TValue>(receiver, callback);
    }

    private static EventCallback<TValue> CreateEventCallback<TValue>(IHandleEvent? receiver, Func<Task> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore<TValue>(receiver, callback);
    }

    private static EventCallback<TValue> CreateEventCallback<TValue>(IHandleEvent? receiver, Func<TValue, Task> callback)
    {
        if (receiver == null)
        {
            throw new ArgumentNullException(nameof(receiver));
        }
        return CreateCore<TValue>(receiver, callback);
    }

    private static EventCallback CreateCore(IHandleEvent receiver, MulticastDelegate callback)
        => new(receiver, callback);

    private static EventCallback<TValue> CreateCore<TValue>(IHandleEvent receiver, MulticastDelegate callback)
        => new(receiver, callback);
}
