﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private readonly List<string> _cachedPermissions = new();
    private readonly List<TItem> _cachedEditedItems = new();
    private readonly List<TItem> _cachedAddItems = new();
    private readonly List<TItem> _cachedAddSelectedItems = new();

    async Task LoadPermissionCallback()
    {
        if (StateOption.IsAccessControl)
        {
            _cachedPermissions.Clear();
            _cachedPermissions.AddRange(await OnLoadPermissionAsync!());
        }
    }

    async Task InlineEditCallback(IGridViewRow row)
    {
        if (OnSaveAsync is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.parameter"),
                I18n.T("$enjoyBlazor.gridView.editor.callbackPrompt", true, null, "OnSaveAsync"),
                AlertTypes.Warning);
        }
        else if (row.EditItem is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.data"),
                I18n.T("$enjoyBlazor.gridView.editor.selectPrompt"),
                AlertTypes.Warning);
        }
        else
        {
            try
            {
                if (await OnSaveAsync(new TItem[] { (TItem)row.EditItem }, false))
                {
                    await row.UpdateEditedAsync();
                }
            }
            catch (Exception ex)
            {
                await PopupService.ConfirmAsync(I18n.T("$enjoyBlazor.confirm.error"), ex.Message, AlertTypes.Error);
            }
        }
    }

    async Task PopupEditCallback(TItem? item)
    {
        if (EditTemplate is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.parameter"),
                I18n.T("$enjoyBlazor.gridView.editor.templatePrompt", true, null, "EditTemplate"),
                AlertTypes.Warning);
        }
        else if (OnSaveAsync is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.parameter"),
                I18n.T("$enjoyBlazor.gridView.editor.callbackPrompt", true, null, "OnSaveAsync"),
                AlertTypes.Warning);
        }
        else if (item is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.data"),
                I18n.T("$enjoyBlazor.gridView.editor.selectPrompt"),
                AlertTypes.Warning);
        }
        else
        {
            var OnConfirmAsync = async (EditContext ctx) =>
            {
                try
                {
                    if (await OnSaveAsync(new TItem[] { item }, false))
                    {
                        await RefreshAsync(false);
                    }
                }
                catch (Exception ex)
                {
                    await PopupService.ConfirmAsync(I18n.T("$enjoyBlazor.confirm.error"), ex.Message, AlertTypes.Error);
                }
            };

            await PopupService.EditDialogAsync<TItem>(new Dictionary<string, object?>() {
                {nameof(EEditDialog<TItem>.CaptureRef),new Action<IRefreshState>(x=> _editForm= x) },
                {nameof(EEditDialog<TItem>.BodyTemplate),EditTemplate },
                {nameof(EEditDialog<TItem>.IsAdd),false },
                {nameof(EEditDialog<TItem>.MaxWidth),PopupDialogWidth },
                {nameof(EEditDialog<TItem>.Model),item },
                {nameof(EEditDialog<TItem>.IsScrolling),true },
                {nameof(EEditDialog<TItem>.Title),I18n.T("$enjoyBlazor.gridView.editor.edit") },
                {nameof(EEditDialog<TItem>.IsKeyboard),true },
                {nameof(EEditDialog<TItem>.ShowLoading),true },
                {nameof(EEditDialog<TItem>.OnConfirmAsync),OnConfirmAsync},
                {nameof(EEditDialog<TItem>.CancelText),I18n.T("$enjoyBlazor.gridView.editor.cancel") },
                {nameof(EEditDialog<TItem>.ConfirmText),I18n.T("$enjoyBlazor.gridView.editor.confirm") },
            });
        }
    }

    async Task PopupAddCallback()
    {
        if (EditTemplate is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.parameter"),
                I18n.T("$enjoyBlazor.gridView.editor.templatePrompt", true, null, "EditTemplate"),
                AlertTypes.Warning);
        }
        else if (OnSaveAsync is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.parameter"),
                I18n.T("$enjoyBlazor.gridView.editor.callbackPrompt", true, null, "OnSaveAsync"),
                AlertTypes.Warning);
        }
        else
        {
            var item = new TItem();
            var OnConfirmAsync = async (EditContext ctx) =>
            {
                try
                {
                    if (await OnSaveAsync(new TItem[] { item }, true))
                    {
                        await RefreshAsync(false);
                    }
                }
                catch (Exception ex)
                {
                    await PopupService.ConfirmAsync(I18n.T("$enjoyBlazor.confirm.error"), ex.Message, AlertTypes.Error);
                }
            };

            await PopupService.EditDialogAsync<TItem>(new Dictionary<string, object?>() {
                {nameof(EEditDialog<TItem>.CaptureRef),new Action<IRefreshState>(x=> _editForm= x) },
                {nameof(EEditDialog<TItem>.BodyTemplate),EditTemplate },
                {nameof(EEditDialog<TItem>.IsAdd),true },
                {nameof(EEditDialog<TItem>.MaxWidth),PopupDialogWidth },
                {nameof(EEditDialog<TItem>.Model),item },
                {nameof(EEditDialog<TItem>.IsScrolling),true },
                {nameof(EEditDialog<TItem>.Title),I18n.T("$enjoyBlazor.gridView.editor.add")},
                {nameof(EEditDialog<TItem>.IsKeyboard),true },
                {nameof(EEditDialog<TItem>.ShowLoading),true },
                {nameof(EEditDialog<TItem>.OnConfirmAsync),OnConfirmAsync},
                {nameof(EEditDialog<TItem>.CancelText),I18n.T("$enjoyBlazor.gridView.editor.cancel") },
                {nameof(EEditDialog<TItem>.ConfirmText),I18n.T("$enjoyBlazor.gridView.editor.confirm") },
            });
        }
    }

    async Task DeleteCallback(IEnumerable<TItem> items)
    {
        if (!items.Any())
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.data"),
                I18n.T("$enjoyBlazor.gridView.editor.selectPrompt"),
                AlertTypes.Warning);
        }
        else if (OnDeleteAsync is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.parameter"),
                I18n.T("$enjoyBlazor.gridView.editor.callbackPrompt", true, null, "OnDeleteAsync"),
                AlertTypes.Warning);
        }
        else
        {
            if (await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.delete"),
                I18n.T("$enjoyBlazor.gridView.editor.deletePrompt"),
                AlertTypes.Warning))
            {
                try
                {
                    if (await OnDeleteAsync(items))
                        await RefreshAsync(false);
                }
                catch (Exception ex)
                {
                    await PopupService.ConfirmAsync(I18n.T("$enjoyBlazor.confirm.error"), ex.Message, AlertTypes.Error);
                }
            }
        }
    }

    async Task ExportCallback()
    {
        try
        {
            if (OnExportAsync is null)
                await PopupService.ConfirmAsync(
                    I18n.T("$enjoyBlazor.confirm.parameter"),
                    I18n.T("$enjoyBlazor.gridView.editor.callbackPrompt", true, null, "OnExportAsync"),
                    AlertTypes.Warning);

            else
            {
                await ShowLoadingAsync();
                await OnExportAsync(new()
                {
                    AllowPaging = AllowPaging,
                    Skip = PagerOption.Skip,
                    Top = ItemsPerPage,
                    FilterKeywords = _filterKeywords,
                    CustomFilters = _filters.Values,
                    Filter = GetFilters(),
                    Orders = GetSorters()
                });
                await HideLoadingAsync();
            }
        }
        catch (Exception ex)
        {
            await PopupService.ConfirmAsync(I18n.T("$enjoyBlazor.confirm.error"), ex.Message, AlertTypes.Error);
        }
    }

    IReadOnlyCollection<string> IGridViewEditor.Permissions => _cachedPermissions;

    async Task IGridViewEditor.HandleOnSaveInlineEdit(IGridViewRow row)
    {
        if (row.ValidateEditForm() == true)
        {
            await InlineEditCallback(row);
        }
    }

    async Task IGridViewEditor.HandleOnActionClick(MouseEventArgs e, MButton button, IGridViewRow? row)
    {
        var confirmed = true;
        if (button.Attributes.TryGetValue("confirm", out object? confirm))
        {
            confirmed = await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.warning"),
                confirm?.ToString(),
                AlertTypes.Warning);
        }
        if (confirmed)
        {
            if (button.OnClick.HasDelegate)
            {
                var args = new GridViewButtonMouseEventArgs(e, Value, row?.Item);
                await button.OnClick.InvokeAsync(args);

                if (button.Attributes.ContainsKey("reload"))
                {
                    await RefreshAsync(false);
                }
            }

            else if (button.Attributes.ContainsKey("add"))
            {
                if (button.Attributes.ContainsKey("inline"))
                {
                    _cachedAddItems.Add(new());
                    await _addSection.SafeChangStateAsync();
                }
                else
                {
                    await PopupAddCallback();
                }
            }

            else if (button.Attributes.ContainsKey("delete"))
            {
                var list = new List<TItem>();
                if (row?.Item is TItem item)
                {
                    list.Add(item);
                }
                else if (Value?.Count > 0)
                {
                    list.AddRange(Value);
                }

                await DeleteCallback(list);
            }

            else if (button.Attributes.ContainsKey("edit"))
            {
                //行内编辑模式
                if (row is not null && button.Attributes.ContainsKey("inline"))
                {
                    await row.UpdateEditingAsync();
                }
                else
                {
                    //工具栏编辑
                    var item = row?.Item as TItem;
                    if (row is null && Value?.Count > 0)
                    {
                        item = Value[0];
                    }
                    await PopupEditCallback(item);
                }
            }

            else if (button.Attributes.ContainsKey("export"))
            {
                await ExportCallback();
            }

            else if (button.Attributes.ContainsKey("reload"))
            {
                await RefreshAsync(false);
            }
        }
    }

    IList IGridViewAdder.AddItems => _cachedAddItems;

    IList IGridViewAdder.AddSelectedItems => _cachedAddSelectedItems;

    async Task IGridViewAdder.InlineAddCallback()
    {
        if (OnSaveAsync is null)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.parameter"),
                I18n.T("$enjoyBlazor.gridView.editor.callbackPrompt", true, null, "OnSaveAsync"),
                AlertTypes.Warning);
        }
        else if (_cachedAddItems.Count == 0)
        {
            await PopupService.ConfirmAsync(
                I18n.T("$enjoyBlazor.confirm.data"),
                I18n.T("$enjoyBlazor.gridView.editor.selectPrompt"),
                AlertTypes.Warning);
        }
        else
        {
            try
            {
                if (await OnSaveAsync(_cachedAddItems, true))
                {
                    await RefreshAsync(false);
                }
            }
            catch (Exception ex)
            {
                await PopupService.ConfirmAsync(I18n.T("$enjoyBlazor.confirm.error"), ex.Message, AlertTypes.Error);
            }
        }

    }
}
