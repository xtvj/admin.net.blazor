﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    Task ShowLoadingAsync() => _loadingSection!.RefreshState(true);

    Task HideLoadingAsync() => _loadingSection!.RefreshState(false);

    async Task RefreshContentAsync()
    {
        if (AllowVirtualization)
        {
            if (_cachedGroupedColumns.Count > 0)
            {
                if (_groupVirtualize is not null)
                {
                    await _groupVirtualize.RefreshDataAsync();
                }
            }
            else if (_virtualize is not null)
            {
                await _virtualize.RefreshDataAsync();
            }
        }
        await _contentSection.SafeChangStateAsync();
    }
}
