﻿using System.Text;

namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private string GetCssClass()
    {
        return new StringBuilder("e-grid-view m-sheet elevation-1")
            .AddClass("theme--dark", "theme--light", IsDark)
            .AddClass(Class)
            .ToString();
    }

    private string GetCssStyle()
    {
        return new StringBuilder()
                 .AddStyle(Style)
                 .ToString();
    }

    private string GetFrozenColumnClass(IGridViewColumn column)
    {
        return column.IsFixed() ? "frozen" : string.Empty;
    }

    private string GetTableCssClass() => new StringBuilder("e-grid-view__table")
            .AddClass("mobile", IsMobile)
            .AddClass("dense", IsDense)
            .AddClass("stripe", Stripe)
            .AddClass("border", Border)
            .AddClass("text-wrap", "text-ellipsis", TextWrap)
            .ToString();

    private string GetTableBodyCssClass()
    {
        return new StringBuilder()
            .AddClass(_props.BodyCssClass)
            .AddClass("d-flex flex-column", IsMobile)
            .ToString();
    }

    private string GetHeaderRowCssStyle(IGridViewColumn column)
    {
        return new StringBuilder()
            .AddStyle(column.HeaderFooterFixedStyle)
            .ToString();
    }

    private string GetHeaderCellCssClass(IGridViewColumn column)
    {
        return new StringBuilder("dropable")
            .AddClass("sortable", column.GetSortable())
            .AddClass("frozen", column.IsFixed())
            .AddClass("border", StateOption.ColumnDeepestLevel > 1)
            .AddClass($"text-{column.GetHeaderAlign().ToString().ToLower()}", "text-center", column.GetCategory() == ColumnType.Data)
            .AddClass(column.GetHeaderCellCssClass())
            .ToString();
    }

    private string GetHeaderCellCssStyle(IGridViewColumn column)
    {
        return new StringBuilder()
            .AddStyle(column.BodyDataCellFixedStyle)
            .ToString();
    }
}
