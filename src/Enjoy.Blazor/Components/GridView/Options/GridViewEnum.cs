﻿namespace Enjoy.Blazor;

public class GridViewEnum<TEnum> where TEnum : Enum
{
    public required string Name { get; set; }

    public required TEnum Value { get; set; }

    public string? Symbol { get; set; }
}