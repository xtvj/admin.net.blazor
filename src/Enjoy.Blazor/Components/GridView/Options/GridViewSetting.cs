﻿namespace Enjoy.Blazor;

public partial class GridViewSetting
{
    /// <summary>
    /// 获取/设置 树展开缩进，默认1rem
    /// </summary>
    public decimal TreeExpandIndent { get; set; }

    /// <summary>
    /// 获取/设置 树展开前图标
    /// </summary>
    public string? TreeExpandableIcon { get; set; }

    /// <summary>
    /// 获取/设置 树展开后图标
    /// </summary>
    public string? TreeExpandedIcon { get; set; }

    /// <summary>
    /// 获取/设置 启用移动显示模式
    /// </summary>
    public bool Mobile { get; set; }

    /// <summary>
    /// 获取/设置 thead 的显示样式
    /// </summary>
    public string? HeadCssClass { get; set; }

    /// <summary>
    /// 获取/设置 tbody 的显示样式
    /// </summary>
    public string? BodyCssClass { get; set; }

    /// <summary>
    /// 获取/设置 tfooter 的显示样式
    /// </summary>
    public string? FooterCssClass { get; set; }

    /// <summary>
    /// 获取/设置 是否默认展开自定义搜索，默认false/不展开
    /// </summary>
    public bool ExpandedCustomFilter { get; set; } = true;
}
