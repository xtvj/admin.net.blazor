﻿namespace Enjoy.Blazor;

/// <summary>
/// 数据节点
/// </summary>
/// <typeparam name="TItem">数据对象</typeparam>
public class GridViewTreeNode<TItem>
{
    private int _level;
    private readonly List<GridViewTreeNode<TItem>> _childNodes;

    public GridViewTreeNode(TItem data)
    {
        _level = 0;
        _childNodes = new();
        Data = data;
    }

    public TItem Data { get; }

    public IEnumerable<TItem>? Children { get; set; }

    internal int Level
    {
        get => _level;
        set => _level = value;
    }

    internal bool Loaded { get; set; }

    internal bool Expanded { get; set; }

    internal GridViewTreeNode<TItem>? Parent { get; set; }

    internal IReadOnlyList<GridViewTreeNode<TItem>> ChildNodes => _childNodes;

    internal Func<Task>? StateHasChanged { get; set; }

    internal void AddChildNode(GridViewTreeNode<TItem> node)
    {
        node.Level = _level + 1;
        node.Parent = this;
        _childNodes.Add(node);
    }
}
