﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private int GetSortIndex(IGridViewColumn column) => _cachedSortedColumns.IndexOf(column) + 1;

    private async Task OnSortCallback(IGridViewColumn column)
    {
        var order = column.GetDirection();

        if (!MultiSort)
        {
            _cachedSortedColumns.Clear();
            foreach (var c in _allColumns)
            {
                c.SetDirection(Direction.None);
            }
        }

        if (order == Direction.Desc)
        {
            column.SetDirection(Direction.None);
            _cachedSortedColumns.Remove(column);
        }
        else
        {
            if (order == Direction.Asc)
            {
                column.SetDirection(Direction.Desc);
            }
            else
            {
                column.SetDirection(Direction.Asc);
            }

            if (!_cachedSortedColumns.Contains(column))
            {
                _cachedSortedColumns.Add(column);
            }
        }

        await RefreshAsync(false);
    }

    private IReadOnlyCollection<Sorter> GetSorters()
    {
        return _cachedSortedColumns
            .Select(x => new Sorter(x.GetPropertyName()!, x.GetDirectionAsString()))
            .ToArray();
    }
}
