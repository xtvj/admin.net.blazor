﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private readonly List<TItem> _view = new(200);

    private bool ComparerItem(TItem a, TItem b) => EqualityComparer is null ? a.Equals(b) : EqualityComparer(a, b);

    /// <summary>
    /// 处理数据节点
    /// </summary>
    /// <param name="resetToFirstPage">重置到第一页</param>
    /// <returns></returns>
    private async Task ProcessDataAsync(bool resetToFirstPage)
    {
        if (_processDataFlag)
        {
            _processDataFlag = false;
        }

        if (_updateValueFlag)
        {
            _updateValueFlag = false;
        }

        if (OnLoadData is null)
        {
            if (_data is not null)
            {
                //本地数据处理
                var query = QueryableExtension.Where(_data.AsQueryable(), GetFilters());
                query = QueryableExtension.OrderBy(query, GetSorters());

                var count = query.Count();
                var skip = resetToFirstPage ? 0 : PagerOption.Skip;
                var currentPage = resetToFirstPage ? 0 : PagerOption.Page;

                if (AllowPaging)
                {
                    if (skip >= PagerOption.TotalRecordCount && PagerOption.TotalRecordCount > PagerOption.ItemsPerPage)
                    {
                        skip = PagerOption.TotalRecordCount - PagerOption.ItemsPerPage;
                    }

                    if (PagerOption.TotalRecordCount <= PagerOption.ItemsPerPage)
                    {
                        skip = 0;
                        currentPage = 0;
                    }

                    query = query.Skip(skip).Take(PagerOption.ItemsPerPage);
                }
                var data = query.ToList();

                PagerOption.TotalRecordCount = count;
                PagerOption.Page = currentPage;
                PagerOption.Skip = skip;

                _cachedTreeNodes.Clear();
                _cachedExpandedItems.Clear();
                _cacheSelectedItems.Clear(); 
                _cachedAddItems.Clear();
                _groupedView = null;
                _view.Clear();

                _view.AddRange(data);
                if (_value is not null)
                {
                    _cacheSelectedItems.AddRange(data.Where(x => _value.Exists(y => ComparerItem(x, y))));
                }
            }
        }
        else
        {
            //远程获取数据
            var skip = resetToFirstPage ? 0 : PagerOption.Skip;
            var currentPage = resetToFirstPage ? 0 : PagerOption.Page;
            var result = await OnLoadData(new()
            {
                AllowPaging = AllowPaging,
                Skip = skip,
                Top = PagerOption.ItemsPerPage,
                FilterKeywords = _filterKeywords,
                CustomFilters = _filters.Values,
                Filter = GetFilters(),
                Orders = GetSorters()
            });

            PagerOption.TotalRecordCount = result.Item1;
            PagerOption.Page = currentPage;
            PagerOption.Skip = skip;
            _data = result.Item2;

            _allSelectedState = SelectedState.UnSelected;
            _cachedTreeNodes.Clear();
            _cachedExpandedItems.Clear();
            _cacheSelectedItems.Clear();
            _cachedAddItems.Clear();
            _groupedView = null;
            _view.Clear();

            _view.AddRange(result.Item2);
            if (_value is not null)
            {
                _cacheSelectedItems.AddRange(result.Item2.Where(x => _value.Exists(y => ComparerItem(x, y))));
            }
        }
    }

    private async Task ReloadDataAsync(bool resetToFirstPage)
    {
        await ProcessDataAsync(resetToFirstPage);
        await UpdateSelectedState(false);
        CalculatePager();
    }

    public int GetViewCount()
    {
        var count = _view.Count;
        if (StateOption.IsTreeGridView)
        {
            foreach (var item in _view)
            {
                GetChildViewRecordCount(ref count, GetTreeNode(item));
            }
        }

        return count;

        static void GetChildViewRecordCount(ref int count, GridViewTreeNode<TItem> node)
        {
            if (node.Expanded)
            {
                count += node.ChildNodes.Count;
                foreach (var child in node.ChildNodes)
                {
                    GetChildViewRecordCount(ref count, child);
                }
            }
        }
    }

    public async Task RefreshAsync(bool resetToFirstPage)
    {
        await ShowLoadingAsync();
        await ReloadDataAsync(resetToFirstPage);
        await RefreshContentAsync();
        await HideLoadingAsync();
    }

    private GridViewTreeNode<TItem> GetTreeNode(TItem item)
    {
        var node = _cachedTreeNodes.FirstOrDefault(x => ComparerItem(x.Data, item));
        if (node is not null)
        {
            return node;
        }
        else
        {
            var newNode = new GridViewTreeNode<TItem>(item);
            //优先使用 TreeNodeConverter
            if (TreeNodeConverter is not null)
            {
                TreeNodeConverter(newNode);
            }
            else if (_childrenGetter is not null)
            {
                var treeNodes = _childrenGetter(item);
                newNode.Children = treeNodes;
            }
            _cachedTreeNodes.Add(newNode);
            return newNode;
        }
    }
}
