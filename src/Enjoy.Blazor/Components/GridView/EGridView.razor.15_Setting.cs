﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private static string GetFrozenIcon((bool, bool) value)
    {
        return value switch
        {
            (true, false) => "mdi-format-horizontal-align-left",
            (false, false) => "mdi-format-horizontal-align-right",
            _ => "mdi-arrow-expand-horizontal",
        };
    }

    private async Task ChangeFilterStateAsync()
    {
        await _settingSection.SafeChangStateAsync();

        if (AllowKeywordsFiltering || AllowCustomFiltering || AllowDynamicFiltering)
        {
            await _topbarSection.SafeChangStateAsync();
        }

        if (AllowHeadFiltering || AllowRowFiltering)
        {
            await _tableSection.SafeChangStateAsync();
        }
    }

    private async void HandleOnSettingClick(IGridViewColumn column, string type)
    {
        if (type == "sort")
        {
            if (column.GetSortable())
            {
                column.SetDirection(Direction.None);
                column.SetSortable(false);
            }
            else
            {
                column.SetSortable(true);
            }
        }
        else if (type == "order")
        {
            column.SetReorderable(!column.GetReorderable());
        }
        else if (type == "size")
        {
            column.SetResizable(!column.GetResizable());
        }
        else if (type == "group")
        {
            column.SetGroupable(!column.GetGroupable());
        }
        else if (type == "frozen")
        {
            var frozen = column.GetFixed();
            if (frozen == (false, false))
            {
                column.SetFixed((true, false));
            }
            else if (frozen == (true, false))
            {
                column.SetFixed((false, true));
            }
            else if (frozen.Tail)
            {
                column.SetFixed((false, false));
            }
        }

        await _tableSection.SafeChangStateAsync();
    }

    private async Task HandleOnEndColumnFilteringDrop(DragCustomEventArgs args)
    {
        if (string.IsNullOrWhiteSpace(args.TransferText))
        {
            return;
        }

        var column = _allColumns.Find(x => x.Identifier == args.TransferText);
        if (column is null)
        {
            return;
        }

        if (!column.GetFilterable() && !_cachedFilterableColumns.Contains(column))
        {
            _cachedFilterableColumns.Add(column);
            column.SetFilterable(true);
            await ChangeFilterStateAsync();
        }
    }

    private async Task HandleOnRemoveFilterableColumn(IGridViewColumn column)
    {
        if (_cachedFilterableColumns.Contains(column))
        {
            _cachedFilterableColumns.Remove(column);
            column.SetFilterable(false);
            await ChangeFilterStateAsync();
        }
    }

    private async Task HandleOnEndColumnSettingDrop(DragCustomEventArgs args)
    {
        if (string.IsNullOrWhiteSpace(args.TransferText))
        {
            return;
        }

        var column = _allColumns.Find(x => x.Identifier == args.TransferText);
        if (column is null)
        {
            return;
        }

        if (column.GetSettable() && !_cachedSettableColumns.Contains(column))
        {
            _cachedSettableColumns.Add(column);
            await _settingSection.SafeChangStateAsync();
        }
    }

    private async Task HandleOnRemoveSettableColumn(IGridViewColumn column)
    {
        if (_cachedSettableColumns.Contains(column))
        {
            _cachedSettableColumns.Remove(column);
            await _settingSection.SafeChangStateAsync();
        }
    }
}
