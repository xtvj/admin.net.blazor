﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private readonly List<TItem> _cacheSelectedItems = new();

    private bool IsRowSelected(TItem item) => _cacheSelectedItems.Exists(x => ComparerItem(x, item));

    private async Task SelectRow(TItem item, bool selected)
    {
        var addItems = new List<TItem>();
        var removeItems = new List<TItem>();

        if (RowSelectMode == Tristate.Single)
        {
            removeItems = _cacheSelectedItems.ToList();
            _cacheSelectedItems.Clear();
        }

        if (selected)
        {
            addItems.Add(item);
            _cacheSelectedItems.Add(item);
        }
        else
        {
            removeItems.RemoveAll(x => ComparerItem(x, item));
            _cacheSelectedItems.RemoveAll(x => ComparerItem(x, item));
        }

        await UpdateSelectedState(true);
        await InvokeSelectedCallback(addItems, removeItems);
    }

    private async Task SelectTreeRow(GridViewTreeNode<TItem> node, bool selected)
    {
        var addItems = new List<TItem>();
        var removeItems = new List<TItem>();

        if (RowSelectMode == Tristate.Single)
        {
            removeItems = _cacheSelectedItems.ToList();
            _cacheSelectedItems.Clear();
        }

        if (selected)
        {
            addItems.Add(node.Data);
            _cacheSelectedItems.Add(node.Data);
        }
        else
        {
            removeItems.RemoveAll(x => ComparerItem(x, node.Data));
            _cacheSelectedItems.RemoveAll(x => ComparerItem(x, node.Data));
        }

        // 选中后，级联更新父及子节点状态
        if (RowSelectMode != Tristate.Single)
        {
            CascadeSelectParent(node, selected);
            CascadeSelectChildren(node, selected);
        }

        await UpdateSelectedState(true);
        await InvokeSelectedCallback(addItems, removeItems);
        await _tableSection.SafeChangStateAsync();

        void CascadeSelectParent(GridViewTreeNode<TItem> current, bool value)
        {
            var parentNode = current.Parent;
            if (parentNode is null)
                return;

            var parentSelected = IsRowSelected(parentNode.Data);
            if (value && !parentSelected)
            {
                var childSelectedCount = parentNode.ChildNodes.Count(x => IsRowSelected(x.Data));
                if (childSelectedCount == parentNode.ChildNodes.Count)
                {
                    addItems.Add(parentNode.Data);
                    _cacheSelectedItems.Add(parentNode.Data);
                    CascadeSelectParent(parentNode, true);
                }
            }
            else if (!value && parentSelected)
            {
                var childSelectedCount = parentNode.ChildNodes.Count(x => IsRowSelected(x.Data));
                if (childSelectedCount < parentNode.ChildNodes.Count)
                {
                    removeItems.RemoveAll(x => ComparerItem(x, parentNode.Data));
                    _cacheSelectedItems.RemoveAll(x => ComparerItem(x, parentNode.Data));
                    CascadeSelectParent(parentNode, false);
                }
            }
        }

        void CascadeSelectChildren(GridViewTreeNode<TItem> current, bool value)
        {
            if (!current.Expanded)
                return;

            foreach (var child in current.ChildNodes)
            {
                if (value)
                {
                    addItems.Add(child.Data);
                    _cacheSelectedItems.Add(child.Data);
                }
                else
                {
                    removeItems.RemoveAll(x => ComparerItem(x, child.Data));
                    _cacheSelectedItems.RemoveAll(x => ComparerItem(x, child.Data));
                }

                CascadeSelectChildren(child, value);
            }
        }
    }

    private async Task SelectAllRow(bool selectedAll)
    {
        var addItems = new List<TItem>();
        var removeItems = new List<TItem>();
        foreach (var item in _view)
        {
            var selected = _cacheSelectedItems.Contains(item);
            if (selected && !selectedAll)
            {
                removeItems.Add(item);
                _cacheSelectedItems.RemoveAll(x => ComparerItem(x, item));

                if (StateOption.IsTreeGridView)
                    CascadeSelectChildren(GetTreeNode(item), false);
            }
            else if (!selected && selectedAll)
            {
                addItems.Add(item);
                _cacheSelectedItems.Add(item);

                if (StateOption.IsTreeGridView)
                    CascadeSelectChildren(GetTreeNode(item), true);
            }
        }

        await UpdateSelectedState(true);
        await InvokeSelectedCallback(addItems, removeItems);
        void CascadeSelectChildren(GridViewTreeNode<TItem> node, bool selected)
        {
            foreach (var child in node.ChildNodes)
            {
                var value = node.Expanded && selected;
                if (value)
                {
                    addItems.Add(child.Data);
                    _cacheSelectedItems.Add(child.Data);
                }
                else
                {
                    removeItems.Add(child.Data);
                    _cacheSelectedItems.RemoveAll(x => ComparerItem(x, child.Data));
                }

                CascadeSelectChildren(child, value);
            }
        }
    }

    private async Task OnSelectedValueChange()
    {
        var selectedItems = new List<TItem>();
        if (_value is not null)
        {
            if (StateOption.IsTreeGridView)
            {
                foreach (var item in _value)
                {
                    var node = _cachedTreeNodes.FirstOrDefault(x => ComparerItem(x.Data, item));
                    if (node is not null && !selectedItems.Exists(x => ComparerItem(x, node.Data)))
                    {
                        if (RowSelectMode == Tristate.Single)
                        {
                            selectedItems.Add(node.Data);
                            break;
                        }
                        else selectedItems.Add(node.Data);
                    }
                }
            }
            else
            {
                foreach (var item in _value)
                {
                    var node = _view.FirstOrDefault(x => ComparerItem(x, item));
                    if (node is not null && !selectedItems.Exists(x => ComparerItem(x, node)))
                    {
                        if (RowSelectMode == Tristate.Single)
                        {
                            selectedItems.Add(node);
                            break;
                        }
                        else selectedItems.Add(node);
                    }
                }
            }
        }

        var addItems = selectedItems.FindAll(x => !_cacheSelectedItems.Exists(y => ComparerItem(y, x)));
        var removeItems = _cacheSelectedItems.FindAll(x => !selectedItems.Exists(y => ComparerItem(y, x)));

        _cacheSelectedItems.RemoveAll(x => removeItems.Exists(y => ComparerItem(y, x)));
        _cacheSelectedItems.AddRange(addItems);

        await UpdateSelectedState(false);
        await InvokeSelectedCallback(addItems, removeItems);
    }

    private async Task UpdateSelectedState(bool effectValue)
    {
        if (_cacheSelectedItems.Count == 0)
        {
            _allSelectedState = SelectedState.UnSelected;
        }
        else
        {
            _allSelectedState = _cacheSelectedItems.Count == GetViewCount()
               ? SelectedState.SelectedAll
               : SelectedState.Indeterminate;
        }

        if (effectValue)
        {
            _value = _cacheSelectedItems.ToList();

            if (ValueChanged.HasDelegate)
            {
                await ValueChanged.InvokeAsync(_value);
            }
            else
            {
                await _tableSection.SafeChangStateAsync();
            }
        }
    }

    private async Task InvokeSelectedCallback(IList<TItem> addItems, IList<TItem> removeItems)
    {
        if (OnRowDeselect is not null && removeItems.Count > 0)
        {
            foreach (var x in removeItems)
                await OnRowDeselect(x);
        }

        if (OnRowSelect is not null && addItems.Count > 0)
        {
            foreach (var x in addItems)
                await OnRowSelect(x);
        }
    }
}
