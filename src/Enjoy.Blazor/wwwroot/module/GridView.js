﻿export function mobileBreak(element, interop, mobileBreakWidth) {
    //宽度监听
    if (mobileBreakWidth > 0) {
        var getWidth = function (el) {
            return el.clientWidth == 0 ? (el.parentElement ? getWidth(el.parentElement) : 0) : el.clientWidth;
        }

        //获取宽度
        var width = getWidth(element);
        var isMobileBreaked = width <= mobileBreakWidth;

        //监听大小
        new ResizeObserver((entries => {
            if (entries.length > 0) {
                var size = entries[0].contentBoxSize[0].inlineSize;
                var breaked = size <= mobileBreakWidth && size > 0;
                if (isMobileBreaked != breaked) {
                    isMobileBreaked = breaked;
                    interop.invokeMethodAsync('Breaked', breaked);
                }
            }
        })).observe(element);

        return isMobileBreaked;
    }

    return false;
}

export function bindCellClick(element, interop) {
    var table = element.querySelector('div.e-grid-view__wrapper>table');
    if (table) table.addEventListener('click', event => {
        let td = event.target.closest('td');
        if (!td || !td.hasAttribute('click')) return;
        if (!table.contains(td)) return;
        let columnId = td.getAttribute('name');
        let rowId = td.parentElement.getAttribute('name');
        interop.invokeMethodAsync('CellClick', rowId, columnId);
    });
}