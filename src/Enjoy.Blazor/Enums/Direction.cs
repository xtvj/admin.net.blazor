﻿using System.ComponentModel;

namespace Enjoy.Blazor;

/// <summary>
/// 排序方向
/// </summary>
public enum Direction
{
    /// <summary>
    /// 不排序
    /// </summary>
    [Description("不排序")]
    None = 0,

    /// <summary>
    /// 从小到达
    /// </summary>
    [Description("从小到大")]
    Asc = 1,

    /// <summary>
    /// 从大到小
    /// </summary>
    [Description("从大到小")]
    Desc = 2,
}
